package net.cqwu.fsboot.dao.adminDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Fruit;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 22:43
 **/
@Repository
public interface FruitMapper extends BaseMapper<Fruit> {
    //根据水果类型查找水果
    @Select("select * from fruits where ftype=#{ftype}")
    List<Fruit> getFruitListByType(String ftype);

    /**
     * 获得某种水果的数量
     *
     * @param type 水果类型
     * @return Integer
     */
    @Select("select count(*) from fruits where ftype = #{param1}")
    Integer getFruitClassNumber(String type);

    /**
     * 根据水果名字查找水果
     * @param fname
     * @return
     */
    @Select("select * from fruits where fname = #{fname}")
    Fruit getFruitByFname(String fname);
    /**
     *查找销量前12的水果
     */
    @Select("select * from fruits order by fsale desc limit 0,12")
    List<Fruit> getFruitBySale();

    /**
     * 假删除
     * @param fid 水果id
     * @return
     */
    @Update("update fruits set fstatus=0 where fid=#{fid}")
    int modifyFstatus(int fid);

    @Select("select fstatus  where fid=#{fid}")
    int SelectfstatusById(int fid);

    @Select("select * from fruits where fstatus=1 ")
    List<Fruit> getFruitList();

    /**
     * 模糊查询
     * @param str 输入的内容
     * @return 返回一个水果list
     */
    @Select("SELECT * from fruits where fname like \"%\"#{fname}\"%\"")
    List<Fruit> fuzzyQueryFruit(String str);
}
