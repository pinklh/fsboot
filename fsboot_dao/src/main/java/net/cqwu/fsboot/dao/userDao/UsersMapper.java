package net.cqwu.fsboot.dao.userDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Users;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 14:59
 **/
@Repository
public interface UsersMapper extends BaseMapper<Users> {
    //用户登录
    @Select("select * from users where utele=#{utele} and upwd=#{upwd}")
    public Users login(String utele,String upwd);
    //用户注册
    @Insert("insert into users(uname,upwd,utele,utype,umoney) values(#{uname},#{upwd},#{utele},0,0)")
    boolean register(String utele,String upwd,String uname);
    //用户通过手机号和姓名查找信息
    @Select("select * from users where utele=#{utele} and uname=#{uname}")
    Users selectByTeleName1(String utele,String uname);

}
