package net.cqwu.fsboot.dao.adminDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Refund;
import net.cqwu.fsboot.pojo.UCoupan;
import org.springframework.stereotype.Repository;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/15 15:14
 * @PackageName:net.cqwu.fsboot.dao.adminDao
 * @ClassName: RefundMapper
 * @Description: TODO
 * @Version 1.0
 */
@Repository
public interface RefundMapper  extends BaseMapper<Refund> {
}
