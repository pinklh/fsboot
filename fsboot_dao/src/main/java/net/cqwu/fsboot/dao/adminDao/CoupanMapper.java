package net.cqwu.fsboot.dao.adminDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Coupan;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 12:52
 **/
@Repository
public interface CoupanMapper extends BaseMapper<Coupan> {
    //查询用户的优惠券
    @Select("select * from coupan where cid in (select cid from u_coupan where uid = #{param1} and cmoney<#{zongjia}) ")
    List<Coupan> getUsersCoupanList(Integer uid,Float zongjia);

    /**
     * 用户使用完优惠券后进行删除
     * @param cid 优惠券id
     * @param uid 用户id
     *
     * @return
     */
    @Delete("delete from u_coupan where cid=#{cid} and uid=#{uid}")
    int delByCidAndUid(int cid,int uid);
    @Select("select cid from coupan where cmoney=#{cmoney} ")
    int getCidByCmoney(int cmoney);
    @Select("select * from coupan where cid in (select cid from u_coupan where uid = #{param1})")
    List<Coupan> getUsersCoupanList1(Integer uid);

}
