package net.cqwu.fsboot.dao.userDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.UMoney;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 19:32
 **/
@Repository
public interface UMoneyMapper extends BaseMapper<UMoney> {
    //查询用户的资金明细
    @Select("select * from u_money where m_uid=#{param1}")
    List<UMoney> getUMoneyList1(Integer muid);

    /**
     * 获取某个月的用户资金流入或流出列表
     *
     * @param year  年
     * @param month 月
     * @param type  类型
     * @return List
     */
    @Select("select m_money from u_money " +
            "where mtime >= '${param1}-${param2}-1' and mtime < '${param1}-${param2 + 1}-1' " +
            "and mtype = #{param3} " +
            "and m_uid = ${param4}")
    List<Float> getMonthUmoney(int year, int month, String type, int uid);

    /**
     * 通过oid和uid查找到用户的消费记录
     * @param oid 订单id
     * @param uid 用户id
     * @return UMoney
     */
    @Select("select * from u_money where oid=#{oid} and m_uid=#${uid}")
    UMoney findByOidAndUid(int oid,int uid);
}
