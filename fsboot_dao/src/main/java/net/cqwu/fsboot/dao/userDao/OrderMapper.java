package net.cqwu.fsboot.dao.userDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Order;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:31
 **/
@Repository
public interface OrderMapper extends BaseMapper<Order> {
    //查询用户订单
    @Select("select * from ordera where uid=#{uid}")
    List<Order> getOrderByUid(int uid);

    /**
     * 获取某个月的销售额集合
     *
     * @param month 月份
     * @return List
     */
    @Select("select orprice from ordera where otime >= '${param1}-${param2}-1' and otime < '${param1}-${param2 + 1}-1' and ostate != 0")
    List<Float> getMonthSales(int year, int month);

    /**
     * 查询用户订单的状态
     */
    @Select("SELECT COUNT(*) FROM ordera WHERE ostate=#{ostate} AND uid =#{uid} ;")
    int orderNumber(int ostate,int uid);



}
