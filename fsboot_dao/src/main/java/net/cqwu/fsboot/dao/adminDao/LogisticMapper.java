package net.cqwu.fsboot.dao.adminDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Logistic;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:03
 **/
@Repository
public interface LogisticMapper extends BaseMapper<Logistic> {
    //根据订单id查找物流
    @Select("select * from logistic where oid=#{oid}")
    Logistic getLogByOid(int oid);
}
