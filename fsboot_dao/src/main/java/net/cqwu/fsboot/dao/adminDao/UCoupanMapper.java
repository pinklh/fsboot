package net.cqwu.fsboot.dao.adminDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.UCoupan;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 18:34
 **/
@Repository
public interface UCoupanMapper extends BaseMapper<UCoupan> {
    @Delete("delete from u_coupan where uid=#{uid} and cid=#{cid}")
    boolean deleteById(int uid,int cid);
    @Select("select * from u_coupan where uid=${uid}")
    List<UCoupan> getUCoupanByIdList(int uid);
    /**
     * 查找用户所拥有的某一张优惠券
     */
    @Select("select count(*) from u_coupan where uid=#{uid} and cid=#{cid}")
    int getCoupanByUidAndCid(int uid,int cid);
}
