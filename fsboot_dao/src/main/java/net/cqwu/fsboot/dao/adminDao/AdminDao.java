package net.cqwu.fsboot.dao.adminDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.Admin;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author 不一样的黎
 * 我就是我，不一样的烟火！
 * @createdTime 2022/6/4 11:00
 */
@Repository
public interface AdminDao extends BaseMapper<Admin> {
    @Select("select * from admin where aname=#{aname}")
    Admin findByName(String aname);
    //管理员登录
    @Select("select * from admin where aname= #{param1} and apwd = #{param2}")
    public Admin login(String aname, String apwd);

    /**
     * 获得会员的数量
     * @return Integer
     */
    @Select("select count(*) from users where utype = #{param1}")
    Integer getUserNumber(Integer utype);
}
