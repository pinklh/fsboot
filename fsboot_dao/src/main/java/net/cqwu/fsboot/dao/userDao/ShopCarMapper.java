package net.cqwu.fsboot.dao.userDao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.cqwu.fsboot.pojo.ShopCar;
import net.cqwu.fsboot.pojo.Users;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/9 19:26
 * @PackageName:net.cqwu.fsboot.dao.userDao
 * @ClassName: ShopCarMapper
 * @Description: TODO
 * @Version 1.0
 */
@Repository
public interface ShopCarMapper extends BaseMapper<ShopCar> {
    @Select("select * from shopcar where uid=#{uid} and fstatus=1")
    List<ShopCar> getShopCarByUid(int uid);
    @Select("select * from shopcar where uid=#{uid} and sstatus=1 ")
    List<ShopCar> getCarBy1(int uid);
//    @Insert("insert into shopcar(fid,uid,furl,fname,sprice,snumber,price,sstatus) values(#{fid},#{uid},#{furl},#{fname},#{sprice},#{snumber},#{price},0)")
//    int add(ShopCar shopCar);
    @Select("select count(*) from shopcar where uid=#{uid} and fid=#{fid}")
    int ifexist(int uid,int fid);

//    更新
    @Update("update shopcar set snumber=#{snumber}  where uid=#{uid} and fid=#{fid}")
    boolean updatecar(int snumber,int uid,int fid);

    @Select("select snumber from shopcar where uid=#{uid} and fid=#{fid}")
    int selectsnumber(int uid,int fid);

    @Update("update shopcar set fstatus=0 where fid=#{fid}")
    boolean updatefstatus(int fid);
}
