package net.cqwu.fsboot.service.userService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Users;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 15:06
 **/

public interface UsersService extends IService<Users> {
    //登录
    public Users login(String utele,String upwd);
    //注册
    boolean register(String utele,String upwd,String uname);
    //用户通过手机号和姓名查找信息
    Users selectByTeleName(String utele,String uname);
//    //添加用户
//    Integer addUsers(Users users);
//    //根据删除用户
//    Integer deleteUsersById(Integer uid);
//    //修改用户
//    Integer updateUsers(Users users);
//    //根据id查询用户信息
//    Users getUsersById(Integer uid);
//    //查询全部老师
//    List<Users> getAllUsers();
//    条件查询
//List<Users> conditionSelectActivity(@Param("uname") String uname, @Param("utype") int utype);
    List<Users> conditionSelect(String uname,int utype);
}
