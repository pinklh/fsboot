package net.cqwu.fsboot.service.userService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Order;
import net.cqwu.fsboot.pojo.ShopCar;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/9 19:32
 * @PackageName:net.cqwu.fsboot.service.userService
 * @ClassName: ShopCarService
 * @Description: TODO
 * @Version 1.0
 */
public interface ShopCarService extends IService<ShopCar> {
    List<ShopCar> getShopCarByUid(int  uid);
    List<ShopCar> getCarBy1(int uid);
//    int add(ShopCar shopCar);
    int ifexist(int uid,int fid);
    boolean updatecar(int snumber,int uid,int fid);
    int selectsnumber(int  uid,int fid);
    boolean updatefstatus(int fid);
}
