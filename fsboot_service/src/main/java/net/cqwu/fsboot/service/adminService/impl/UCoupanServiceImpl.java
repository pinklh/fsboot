package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.UCoupanMapper;
import net.cqwu.fsboot.pojo.UCoupan;
import net.cqwu.fsboot.service.adminService.UCoupanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 18:37
 **/
@Service
public class UCoupanServiceImpl extends ServiceImpl<UCoupanMapper, UCoupan> implements UCoupanService {
    @Autowired
    private UCoupanMapper U_coupanMapper;
    @Override
    public boolean deleteById(int uid, int cid) {
        return U_coupanMapper.deleteById(uid, cid);
    }

    @Override
    public List<UCoupan> getUCoupanByIdList(int uid) {
        return U_coupanMapper.getUCoupanByIdList(uid);
    }

    @Override
    public int getCoupanByUidAndCid(int uid, int cid) {
        return U_coupanMapper.getCoupanByUidAndCid(uid,cid);
    }
}
