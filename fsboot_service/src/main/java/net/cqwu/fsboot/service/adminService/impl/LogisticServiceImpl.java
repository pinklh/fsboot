package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.LogisticMapper;
import net.cqwu.fsboot.pojo.Logistic;
import net.cqwu.fsboot.service.adminService.LogisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:05
 **/
@Service
public class LogisticServiceImpl extends ServiceImpl<LogisticMapper, Logistic> implements LogisticService {
    @Autowired
    private LogisticMapper logisticMapper;
    @Override
    public Logistic  getLogByOid(int oid) {
        return logisticMapper.getLogByOid(oid);
    }
}
