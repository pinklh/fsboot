package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.AMoneyMapper;
import net.cqwu.fsboot.pojo.AMoney;
import net.cqwu.fsboot.service.adminService.AMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/5 11:24
 **/
@Service
public class AMoneyServiceImpl extends ServiceImpl<AMoneyMapper, AMoney> implements AMoneyService {
    @Autowired
    private AMoneyMapper  aMoneyMapper;

    @Override
    public float getAllMoenySum() {
        List<AMoney> aMoneyList=aMoneyMapper.selectList(null);
        float sum = 0;
        for (AMoney a : aMoneyList) {
            sum += a.getMoney();
        }
        return sum;
    }
}
