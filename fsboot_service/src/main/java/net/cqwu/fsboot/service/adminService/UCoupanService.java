package net.cqwu.fsboot.service.adminService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.UCoupan;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 18:34
 **/

public interface UCoupanService extends IService<UCoupan> {
    boolean deleteById(int uid,int cid);
    List<UCoupan> getUCoupanByIdList(int uid);
    int getCoupanByUidAndCid(int uid,int cid);
}
