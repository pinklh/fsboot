package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.AdminDao;
import net.cqwu.fsboot.pojo.Admin;
import net.cqwu.fsboot.service.adminService.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 不一样的黎
 * 我就是我，不一样的烟火！
 * @createdTime 2022/6/4 11:09
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminDao, Admin> implements AdminService {
    @Resource
    private AdminDao adminDao;
    @Override
    public Admin findByName(String aname) {
        return adminDao.findByName(aname);
    }

    @Override
    public Admin login(String aname, String apwd) {
        return adminDao.login(aname,apwd);
    }
    /**
     * 获得会员的数量
     * @return Integer
     */
    @Override
    public Integer getUserNumber(Integer utype) {
        return adminDao.getUserNumber(utype);
    }
}
