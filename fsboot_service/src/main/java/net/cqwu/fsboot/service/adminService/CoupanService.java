package net.cqwu.fsboot.service.adminService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Coupan;

import java.util.List;


/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 12:53
 **/

public interface CoupanService extends IService<Coupan> {
    //查看用户的优惠券
    List<Coupan> getUsersCoupanList(Integer uid,Float zongjia);

    List<Coupan> getUsersCoupanList1(Integer uid);

    /**
     * 使用完优惠券后删除
     * @param cid
     * @param uid
     * @return
     */
    int delByCidAndUid(int cid,int uid);


    int getCidByCmoney(int cmoney);
}
