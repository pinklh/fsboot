package net.cqwu.fsboot.service.userService.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.userDao.UsersMapper;
import net.cqwu.fsboot.pojo.Users;
import net.cqwu.fsboot.service.userService.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 15:07
 **/
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {
    @Autowired
    private UsersMapper usersMapper;
    @Override
    public Users login(String utele, String upwd) {
        return usersMapper.login(utele, upwd);
    }

    @Override
    public boolean register(String utele, String upwd, String uname) {
        return usersMapper.register(utele, upwd, uname);
    }
    //用户通过手机号和姓名查找信息
    @Override
    public Users selectByTeleName(String utele, String uname) {
        return usersMapper.selectByTeleName1(utele, uname);
    }

    @Override
    public List<Users> conditionSelect(String uname,int utype) {
        System.out.println("utype = " + utype);
        QueryWrapper<Users> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("uname",uname);
        if (utype!=2){
            queryWrapper.like("utype",utype);
        }

        return usersMapper.selectList(queryWrapper);
    }

//    @Override
//    public List<Users> conditionSelectActivity(String uname, int utype) {
//        return usersMapper.conditionSelectActivity(uname,utype);
//    }
//    @Autowired
//    private UsersMapper usersMapper;
//    //添加用户
//    @Override
//    public Integer addUsers(Users users) {
//        return usersMapper.insert(users);
//    }
//    //根据id删除用户
//    @Override
//    public Integer deleteUsersById(Integer uid) {
//        return usersMapper.deleteById(uid);
//    }
//    //修改用户信息
//    @Override
//    public Integer updateUsers(Users users) {
//        return usersMapper.updateById(users);
//    }
//    //根据id查询用户
//    @Override
//    public Users getUsersById(Integer uid) {
//        return usersMapper.selectById(uid);
//    }
//      //查找所有用户
//    @Override
//    public List<Users> getAllUsers() {
//        return usersMapper.selectList(null);
//    }

}
