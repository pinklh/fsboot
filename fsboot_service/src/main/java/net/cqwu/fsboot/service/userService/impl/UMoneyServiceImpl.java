package net.cqwu.fsboot.service.userService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.userDao.UMoneyMapper;
import net.cqwu.fsboot.pojo.UMoney;
import net.cqwu.fsboot.service.userService.UMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 19:34
 **/
@Service
public class UMoneyServiceImpl extends ServiceImpl<UMoneyMapper, UMoney> implements UMoneyService {
    @Autowired
    private UMoneyMapper uMoneyMapper;

    @Override
    public List<UMoney> getUMoneyList1(Integer uid) {
        return uMoneyMapper.getUMoneyList1(uid);
    }

    /**
     * 获取用户某个月资金的流入或流出总数
     *
     * @param year  年
     * @param month 月
     * @param type  类型
     * @return Float
     */
    @Override
    public Float getOneMonthUmoney(int year, int month, String type, int uid) {
        List<Float> umoneyList = uMoneyMapper.getMonthUmoney(year, month, type, uid);
        Float sum = 0.0f;
        for (Float money : umoneyList) {
            sum += money;
        }
        return sum;
    }

}
