package net.cqwu.fsboot.service.adminService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Admin;

/**
 * @author 不一样的黎
 * 我就是我，不一样的烟火！
 * @createdTime 2022/6/4 11:07
 */
public interface AdminService extends IService<Admin> {
    Admin findByName(String aname);
    Admin login(String aname, String apwd);

    /**
     * 获得会员的数量
     * @return Integer
     */
    Integer getUserNumber(Integer utype);
}
