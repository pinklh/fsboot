package net.cqwu.fsboot.service.userService.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.userDao.ShopCarMapper;
import net.cqwu.fsboot.dao.userDao.UMoneyMapper;
import net.cqwu.fsboot.pojo.ShopCar;
import net.cqwu.fsboot.pojo.UMoney;
import net.cqwu.fsboot.service.userService.ShopCarService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/9 19:32
 * @PackageName:net.cqwu.fsboot.service.userService.impl
 * @ClassName: ShopCarServiceImpl
 * @Description: TODO
 * @Version 1.0
 */
@Service
public class ShopCarServiceImpl extends ServiceImpl<ShopCarMapper, ShopCar> implements ShopCarService {
    @Resource
    private ShopCarMapper shopCarMapper;
    @Override
    public List<ShopCar> getShopCarByUid(int uid) {
        return shopCarMapper.getShopCarByUid(uid);
    }

    @Override
    public List<ShopCar> getCarBy1(int uid) {
        return shopCarMapper.getCarBy1(uid);
    }

    @Override
    public int ifexist(int uid, int fid) {
        return shopCarMapper.ifexist(uid,fid);
    }

    @Override
    public boolean updatecar(int snumber, int uid, int fid) {
        return shopCarMapper.updatecar(snumber,uid,fid);
    }

    @Override
    public int selectsnumber(int uid, int fid) {
        return shopCarMapper.selectsnumber(uid,fid);
    }

    @Override
    public boolean updatefstatus(int fid) {
        return shopCarMapper.updatefstatus(fid);
    }

//    @Override
//    public int add(ShopCar shopCar) {
//        return shopCarMapper.add(shopCar);
//    }
}
