package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.FruitMapper;
import net.cqwu.fsboot.pojo.Fruit;
import net.cqwu.fsboot.pojo.Users;
import net.cqwu.fsboot.service.adminService.FruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 22:46
 **/
@Service
public class FruitServiceImpl extends ServiceImpl<FruitMapper, Fruit> implements FruitService {
    @Autowired
    private FruitMapper fruitMapper;
    @Override
    public List<Fruit> getFruitListByType(String ftype) {
        return fruitMapper.getFruitListByType(ftype);
    }

    @Override
    public List<Fruit> conditionSelect(String ftype, int fprice) {
        QueryWrapper<Fruit> queryWrapper = new QueryWrapper<>();
        if(!ftype.equals("全部")){
            queryWrapper.eq("ftype",ftype);
        }
            queryWrapper.gt("fprice",fprice);

        return fruitMapper.selectList(queryWrapper);
    }

    /**
     * 获得某种水果的数量
     *
     * @param type 水果类型
     * @return Integer
     */
    @Override
    public Integer getFruitClassNumber(String type) {
        return fruitMapper.getFruitClassNumber(type);
    }

    /**
     * 根据水果名字查找水果
     * @param fname
     * @return
     */
    @Override
    public Fruit getFruitByFname(String fname) {
        return fruitMapper.getFruitByFname(fname);
    }

    @Override
    public List<Fruit> getFruitBySale() {
        return fruitMapper.getFruitBySale();
    }

    @Override
    public int modifyFstatus(int fid) {
        return fruitMapper.modifyFstatus(fid);
    }

    @Override
    public int SelectfstatusById(int fid) {
        return fruitMapper.SelectfstatusById(fid);
    }

    @Override
    public List<Fruit> getFruitList() {
        return fruitMapper.getFruitList();
    }

    @Override
    public List<Fruit> fuzzyQueryFruit(String str) {
        return fruitMapper.fuzzyQueryFruit(str);
    }

}
