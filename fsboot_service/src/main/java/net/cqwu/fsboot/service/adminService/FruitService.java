package net.cqwu.fsboot.service.adminService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Fruit;

import java.util.List;
import java.util.Map;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 22:45
 **/

public interface FruitService extends IService<Fruit> {
    //根据水果类型查找水果
    List<Fruit> getFruitListByType(String ftype);
//    多条件查询
    List<Fruit> conditionSelect(String ftype,int fprice);

    /**
     * 获得某种水果的数量
     * @param type 水果类型
     * @return Integer
     */
    Integer getFruitClassNumber(String type);

    /**
     * 付款时根据订单中水果的名字查找到水果
     * @param fname
     * @return
     */
    Fruit getFruitByFname(String fname);

    List<Fruit> getFruitBySale();

    int modifyFstatus(int fid);

    int SelectfstatusById(int fid);

    List<Fruit> getFruitList();

    /**
     * 水果的模糊查询
     * @param str
     * @return
     */
    List<Fruit> fuzzyQueryFruit(String str);

}
