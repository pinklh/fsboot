package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.CoupanMapper;
import net.cqwu.fsboot.pojo.Coupan;
import net.cqwu.fsboot.service.adminService.CoupanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 12:59
 **/
@Service
public class CoupanServiceImpl extends ServiceImpl<CoupanMapper, Coupan> implements CoupanService {
    @Autowired
    private CoupanMapper coupanMapper;
    @Override
    //查看用户的优惠券
    public List<Coupan> getUsersCoupanList(Integer uid,Float zongjia) {
        return coupanMapper.getUsersCoupanList(uid,zongjia);
    }

    @Override
    public List<Coupan> getUsersCoupanList1(Integer uid) {
        return coupanMapper.getUsersCoupanList1(uid);
    }

    @Override
    public int delByCidAndUid(int cid, int uid) {
        return coupanMapper.delByCidAndUid(cid,uid);
    }

    @Override
    public int getCidByCmoney(int cmoney) {
        return coupanMapper.getCidByCmoney(cmoney);
    }
}
