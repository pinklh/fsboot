package net.cqwu.fsboot.service.adminService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Refund;
import net.cqwu.fsboot.pojo.UCoupan;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/15 15:15
 * @PackageName:net.cqwu.fsboot.service.adminService
 * @ClassName: RefundService
 * @Description: TODO
 * @Version 1.0
 */
public interface RefundService extends IService<Refund> {
}
