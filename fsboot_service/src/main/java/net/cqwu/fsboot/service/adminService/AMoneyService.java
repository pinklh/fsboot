package net.cqwu.fsboot.service.adminService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.AMoney;

import java.util.Date;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/5 11:23
 **/

public interface AMoneyService extends IService<AMoney> {
    float getAllMoenySum();
}
