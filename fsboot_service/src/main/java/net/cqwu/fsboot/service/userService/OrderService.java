package net.cqwu.fsboot.service.userService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.Order;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:33
 **/

public interface OrderService extends IService<Order> {
    List<Order> getOrderByUid(int uid);

    /**
     * 获取某个月的销售额
     * @param month 月份
     * @return
     */
    Float getMonthSales(int year, int month);

    /**
     * 获取用户不同订单状态
     * @param ostate 状态
     * @param uid 用户id
     * @return
     */
    int orderNumber(int ostate,int uid);
}
