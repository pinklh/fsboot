package net.cqwu.fsboot.service.adminService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.adminDao.RefundMapper;
import net.cqwu.fsboot.dao.adminDao.UCoupanMapper;
import net.cqwu.fsboot.pojo.Refund;
import net.cqwu.fsboot.pojo.UCoupan;
import net.cqwu.fsboot.service.adminService.RefundService;
import net.cqwu.fsboot.service.adminService.UCoupanService;
import org.springframework.stereotype.Service;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/15 15:16
 * @PackageName:net.cqwu.fsboot.service.adminService.impl
 * @ClassName: RefundServiceImpl
 * @Description: TODO
 * @Version 1.0
 */
@Service
public class RefundServiceImpl extends ServiceImpl<RefundMapper, Refund> implements RefundService {
}
