package net.cqwu.fsboot.service.userService.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.cqwu.fsboot.dao.userDao.OrderMapper;
import net.cqwu.fsboot.pojo.Order;
import net.cqwu.fsboot.service.userService.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:34
 **/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
    @Resource
    private OrderMapper orderMapper;
    @Override
    public List<Order> getOrderByUid(int uid) {
        return orderMapper.getOrderByUid(uid);
    }

    /**
     * 获取某个月的销售额
     *
     * @param month 月份
     * @return Float
     */
    @Override
    public Float getMonthSales(int year, int month) {
        List<Float> monthSales = orderMapper.getMonthSales(year, month);
        Float sum = 0.0f;
        for (Float money: monthSales) {
            sum += money;
        }
        return sum;
    }

    @Override
    public int orderNumber(int ostate, int uid) {
        return orderMapper.orderNumber(ostate,uid);
    }
}
