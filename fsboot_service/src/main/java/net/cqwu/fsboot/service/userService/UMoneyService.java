package net.cqwu.fsboot.service.userService;

import com.baomidou.mybatisplus.extension.service.IService;
import net.cqwu.fsboot.pojo.UMoney;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 19:33
 **/

public interface UMoneyService extends IService<UMoney> {
    //查看用户的资金明细
    List<UMoney> getUMoneyList1(Integer uid);

    /**
     * 获取用户某个月资金的流入或流出总数
     *
     * @param year  年
     * @param month 月
     * @param type  类型
     * @return Float
     */
    Float getOneMonthUmoney(int year, int month, String type, int uid);
}
