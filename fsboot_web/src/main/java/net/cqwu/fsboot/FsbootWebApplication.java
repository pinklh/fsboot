package net.cqwu.fsboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("net.cqwu.fsboot.dao")
@EnableTransactionManagement
public class FsbootWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(FsbootWebApplication.class, args);
    }


}
