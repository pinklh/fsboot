package net.cqwu.fsboot.controller.adminController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.cqwu.fsboot.pojo.Fruit;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.FruitService;
import net.cqwu.fsboot.service.userService.ShopCarService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.ListResourceBundle;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 22:48
 **/
@RestController
@RequestMapping("/fruit")
@CrossOrigin(allowedHeaders = "*", value = "*", originPatterns = "*")
public class FruitController {
    @Autowired
    private FruitService fruitService;
    @Resource
    private ShopCarService shopCarService;

    //查询水果列表
    @GetMapping("/getFruitList")
    public List<Fruit> getFruitList() {
        System.out.println("getFruitList------");
        return fruitService.getFruitList();
    }

    //根据水果类型查找水果
    @GetMapping("/getFruitListByType/{ftype}")
    public List<Fruit> getFruitListByType(@PathVariable("ftype") String ftype) {
        System.out.println("getFruitListByType------" + ftype);
        return fruitService.getFruitListByType(ftype);
    }

    //添加水果（插入）
    @PostMapping("/addFruit")
    public boolean addFruit(@RequestBody Fruit fruit) {
        System.out.println("addFruit------" + fruit);
        fruitService.save(fruit);
        return true;
    }

    //修改水果信息
    @PutMapping("/updateFruit")
    public boolean updateFruit(@RequestBody Fruit fruit) {
        System.out.println("updateFruit------" + fruit);
        return fruitService.updateById(fruit);
    }

    //删除水果
    @DeleteMapping("/deleteFruit/{fid}")
    public int deleteFruit(@PathVariable("fid") int fid) {
        shopCarService.updatefstatus(fid);
        System.out.println("deleteFruit-----" + fid);
        return  fruitService.modifyFstatus(fid);
    }

    //根据id查找水果
    @GetMapping("/getFruitById/{fid}")
    public ResponseBody<Fruit> getFruitById(@PathVariable("fid") int fid) {
        int status = -1;
        String message = "未知错误";
        Fruit fruit = fruitService.getById(fid);
        if (fruit != null) {
            status = 0;
        }
        System.out.println("getFruitById------" + fruit);
        return new ResponseBody<>(status, message, fruit);
    }

    //    多条件查询
    @RequestMapping("/conditionSelect")
    public List<Fruit> conditionSelectActivity(String ftype, int fprice) {
        System.out.println("ftype = " + ftype);
        List<Fruit> fruits = fruitService.conditionSelect(ftype, fprice);
        System.out.println(fruits);
        return fruits;
    }

    /**
     * 获得水果分类
     *
     * @return Map<Integer, String>
     */
    @GetMapping("/getSixFruitClass")
    public List<Integer> getSixFruitClass() {
        String[] fruitTypeName = {"瓜果类", "浆果类", "坚果类", "柑橘类", "核果类"};
        List<Integer> list = new ArrayList<>();
        for (String ftypeName : fruitTypeName) {
            list.add(fruitService.getFruitClassNumber(ftypeName));
        }
        return list;
    }

    @RequestMapping("/getFruitBySale")
     public List<Fruit> getFruitBySale(){
        return fruitService.getFruitBySale();
    }

    @GetMapping("/fuzzyQueryFruit/{fname}")
    public List<Fruit> fuzzyQueryFruit(@PathVariable("fname") String s){
        System.out.println("s = " + s);
        List<Fruit> fruitList = fruitService.fuzzyQueryFruit(s);
        return fruitList;


    }
}
