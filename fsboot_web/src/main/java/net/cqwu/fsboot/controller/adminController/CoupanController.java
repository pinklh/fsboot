package net.cqwu.fsboot.controller.adminController;

import net.cqwu.fsboot.pojo.Coupan;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.CoupanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 13:01
 **/
@RestController
@RequestMapping("/coupan")
@CrossOrigin(allowedHeaders="*",value="*",originPatterns="*")
@Transactional
public class CoupanController {
    @Autowired
    private CoupanService coupanService;
    //查询优惠券列表
    @GetMapping("/getCoupanList")
    public List<Coupan> getCoupanList() {
        System.out.println("getCoupanList-----");
        return coupanService.list();
    }
    //查询用户的优惠券
    @GetMapping("/getUsersCoupanList/{uid}/{zongjia}")
    public List<Coupan> getUsersCoupanList(@PathVariable("uid")Integer uid,@PathVariable("zongjia") Float zongjia){
        System.out.println("uid = " + uid);
        System.out.println("zongjia = " + zongjia);
        System.out.println("getUsersCoupanList------");
        return coupanService.getUsersCoupanList(uid,zongjia);
    }
    @GetMapping("/getUsersCoupanList1/{uid}")
    public List<Coupan> getUsersCoupanList(@PathVariable("uid")Integer uid){
        System.out.println("uid = " + uid);

        System.out.println("getUsersCoupanList------");
        return coupanService.getUsersCoupanList1(uid);
    }
    //添加优惠券
    @PostMapping("/addCoupan")
    public boolean addCoupan(@RequestBody Coupan  coupan){
        System.out.println("addCoupan-----"+coupan);
        return coupanService.save(coupan);
    }
    //修改优惠券
    @PutMapping("/updateCoupan")
    public boolean updateCoupan(@RequestBody Coupan coupan){
        System.out.println("updateCoupan--------"+coupan);
        return coupanService.updateById(coupan);
    }
    //删除优惠券
    @DeleteMapping("/deleteCoupan/{cid}")
    public boolean deleteCoupan(@PathVariable("cid")int cid){
        System.out.println("deleteCoupan"+cid);
        return coupanService.removeById(cid);
    }
    //根据id查找优惠券
    @GetMapping("/getCoupanById/{cid}")
    public ResponseBody<Coupan> getCoupanById(@PathVariable("cid")int cid){
        int status=-1;
        String message="未知错误";
        Coupan coupan = coupanService.getById(cid);
        if(coupan!=null){
            status=0;
        }
        System.out.println("getCoupanById------"+coupan);
        return new ResponseBody<>(status,message,coupan);
    }
}
