package net.cqwu.fsboot.controller.adminController;

import net.cqwu.fsboot.pojo.Admin;
import net.cqwu.fsboot.pojo.Logistic;
import net.cqwu.fsboot.pojo.Order;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.AdminService;
import net.cqwu.fsboot.service.adminService.LogisticService;
import net.cqwu.fsboot.service.userService.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/5 15:51
 **/
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Resource
    private OrderService orderService;
    @Resource
    private LogisticService logisticService;
    @GetMapping("/findByName/{aname}")
    public Integer findByName(@PathVariable("aname")String aname){
        Admin admin = adminService.findByName(aname);
        return admin.getAid();
    }
    //登录
    @RequestMapping("/adminLogin")
    public ResponseBody<Admin> login(@RequestParam String aname,
                                     @RequestParam String apwd,
                                     HttpServletRequest request){
        Admin admin = adminService.login(aname, apwd);
        int status=-1;
        String message = "用户名或密码错误";
        if(admin!=null && admin.getAid()>0){
            //登录成功
            status=0;
            message="";
            //将对象保存在session中
            HttpSession session = request.getSession();
            session.setAttribute("admin",admin);
        }
        return new ResponseBody<>(status,message,admin);
    }
    //根据id查询管理员信息
    @GetMapping("/getAdminById/{aid}")
    public ResponseBody<Admin> getAdminById(@PathVariable("aid")int aid){
        int status =-1;
        String message = "用户名或者密码错误";
        Admin admin =adminService.getById(aid);
        if(admin == null){
            status=0;
            message="登录成功";
        }else{
            status=1;
        }
        System.out.println("getAdminById------"+admin);
        return new ResponseBody<>(status,message,admin);
    }


    @PutMapping("/updateAdmin")
    public boolean updateAdmin(@RequestBody Admin admin){
        System.out.println("updateAdmin------"+admin);
        return adminService.updateById(admin);
    }
    @GetMapping("/fahuo/{oid}")
    public String fahuo(@PathVariable("oid") Integer oid){
        Order order = new Order();
        Logistic logistic = new Logistic();
        order=orderService.getById(oid);
        logistic.setOid(order.getOid());
        logistic.setOaddress(order.getOmessage());
        logistic.setOmessage(order.getOnote());
        logistic.setOstate(0);
        logisticService.save(logistic);
        order.setOstate(2);
        orderService.updateById(order);
        return "true";
    }

    /**
     * 获得会员的数量
     * @return Integer
     */
    @GetMapping("getUserNumber")
    public List<Integer> getUserNumber(){
        List<Integer> userNumList = new ArrayList<>();
        userNumList.add(adminService.getUserNumber(0));
        userNumList.add(adminService.getUserNumber(1));
        return userNumList;
    }


}
