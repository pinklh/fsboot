package net.cqwu.fsboot.controller.adminController;

import net.cqwu.fsboot.pojo.*;
import net.cqwu.fsboot.service.adminService.FruitService;
import net.cqwu.fsboot.service.adminService.RefundService;
import net.cqwu.fsboot.service.userService.OrderService;
import net.cqwu.fsboot.service.userService.UMoneyService;
import net.cqwu.fsboot.service.userService.UsersService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/15 15:18
 * @PackageName:net.cqwu.fsboot.controller.adminController
 * @ClassName: RefundController
 * @Description: TODO
 * @Version 1.0
 */
@RestController
@RequestMapping("/refund")
@CrossOrigin(allowedHeaders="*",value="*",originPatterns="*")
public class RefundController {
    @Resource
    private RefundService refundService;
    @Resource
    private UsersService usersService;
    @Resource
    private UMoneyService uMoneyService;
    @Resource
    private OrderService orderService;
    @Resource
    private FruitService fruitService;
    @RequestMapping("/findAll")
    public List<Refund> findAll(){
        return refundService.list();
    }

    /**
     * 用户点击退款按钮后 将其加入表
     * @param refund
     * @return
     */
    @PostMapping("/addRefund")
    public String addRefund(@RequestBody Refund refund){
        Order order = orderService.getById(refund.getOid());
        order.setOstate(4);
        orderService.updateById(order);
        refundService.save(refund);
        return "成功";
    }
    @PostMapping("/tuikuan")
    public String tuikuan(@RequestBody Refund refund){
        refund.setRstatus(1);
        refundService.updateById(refund);
        Users user = usersService.getById(refund.getUid());
        UMoney uMoney = new UMoney();
        Order order = orderService.getById(refund.getOid());
        Fruit fruit = fruitService.getFruitByFname(order.getOnote());
        int num = (int) (order.getOprice()/fruit.getFprice());
        fruit.setFsale(fruit.getFsale()-num);
        fruit.setFstock(fruit.getFstock()+num);
        fruitService.updateById(fruit);
        user.setUmoney(user.getUmoney()+refund.getRmoney());
        usersService.updateById(user);
        uMoney.setMmoney(refund.getRmoney());
        uMoney.setMuid(refund.getUid());
        String str = user.getUname()+"退款";
        uMoney.setMtype("流入");
        uMoney.setMnote(str);
        Date date = new Date();
        uMoney.setMtime(date);
        uMoneyService.save(uMoney);
        order.setOstate(5);
        orderService.updateById(order);
        return "成功";
    }
}
