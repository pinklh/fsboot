package net.cqwu.fsboot.controller.userController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.cqwu.fsboot.pojo.Order;
import net.cqwu.fsboot.pojo.ShopCar;
import net.cqwu.fsboot.service.userService.ShopCarService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/9 19:39
 * @PackageName:net.cqwu.fsboot.controller.userController
 * @ClassName: ShopCarController
 * @Description: TODO
 * @Version 1.0
 */
@RestController
@RequestMapping("/shopcar")
@CrossOrigin(allowedHeaders="*",value="*",originPatterns="*")
public class ShopCarController {
    @Resource
    private ShopCarService shopCarService;
    @RequestMapping("/findAll")
    public List<ShopCar> findAll(){
        return shopCarService.list();
    }
    @GetMapping("/myCar/{uid}")
    public List<ShopCar> myCar(@PathVariable("uid") int uid){
        return shopCarService.getShopCarByUid(uid);
    }
    @GetMapping("/delCar/{sid}")
    public String delCar(@PathVariable("sid") int sid){
        shopCarService.removeById(sid);
        return "成功";
    }
    @CrossOrigin
    @GetMapping("/save")
    public String save(@RequestParam int fid,
                       @RequestParam int uid,
                       @RequestParam String furl,
                       @RequestParam String fname,
                       @RequestParam float sprice,
                       @RequestParam int snumber,
                       @RequestParam float price) {
        ShopCar shopCar = new ShopCar();
//        /shopCar.setSid(2);

        int i = shopCarService.ifexist(uid,fid);
        System.out.println(i);
        if(i==0){
            shopCar.setFid(fid);
            shopCar.setUid(uid);
            shopCar.setFurl(furl);
            shopCar.setFname(fname);
            shopCar.setSprice(sprice);
            shopCar.setSnumber(snumber);
            shopCar.setPrice(price);
            System.out.println("shopCar = " + shopCar);

            shopCarService.save(shopCar);

        }else {
            int num = shopCarService.selectsnumber(uid, fid);
            shopCarService.updatecar(snumber+num,uid,fid);
        }
        String mess = "成功加入购物车";
        return mess;
    }
    @GetMapping("/ifexist/{uid}/{fid}")
    public int ifexist(@PathVariable("uid") int uid,@PathVariable("fid") int fid){
        int flag = shopCarService.ifexist(uid,fid);
        return flag;
    }



}
