package net.cqwu.fsboot.controller.userController;

import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.pojo.UMoney;
import net.cqwu.fsboot.service.userService.UMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 19:35
 **/
@RestController
@RequestMapping("/umoney")
@CrossOrigin(allowedHeaders = "*", value = "*", originPatterns = "*")
@Transactional
public class UMoneyController {
    @Autowired
    private UMoneyService uMoneyService;

    //查询资金列表
    @GetMapping("/getUMoneyList")
    public List<UMoney> getUMoneyList() {
        System.out.println("getUMoneyList----");
        return uMoneyService.list();
    }

    //查看用户的资金列表
    @GetMapping("/getUMoneyList1/{uid}")
    public List<UMoney> getUMoneyList1(@PathVariable("uid") int uid) {
        System.out.println("getUMoneyList1----" + uid);
        return uMoneyService.getUMoneyList1(uid);
    }

    //添加资金
    @PostMapping("/addUMoney")
    public boolean addUMoney(@RequestBody UMoney uMoney) {
        System.out.println("addUMoney------" + uMoney);
        return uMoneyService.save(uMoney);
    }

    //修改资金信息
    @PutMapping("/updateUMoney")
    public boolean updateUMoney(@RequestBody UMoney uMoney) {
        System.out.println("updateUMoney-----" + uMoney);
        return uMoneyService.updateById(uMoney);
    }

    //删除资金
    @DeleteMapping("/deleteUMoney/{mid}")
    public boolean deleteUMoney(@PathVariable("mid") int mid) {
        System.out.println("deleteUMoney-----" + mid);
        return uMoneyService.removeById(mid);
    }

    //根据id查询资金信息
    @GetMapping("/getUMoneyById/{mid}")
    public ResponseBody<UMoney> getUMoneyById(@PathVariable("mid") int mid) {
        int status = -1;
        String message = "未知错误";
        UMoney uMoney = uMoneyService.getById(mid);
        if (uMoney != null) {
            status = 0;
        }
        System.out.println("getUMoneyById-----" + uMoney);
        return new ResponseBody<>(status, message, uMoney);
    }

    /**
     * 获得半年来用户资金记录
     *
     * @param year  年
     * @param month 月
     * @return List
     */
    @GetMapping("/getUserMoneyRecords")
    public List<List<String>> getUserMoneyRecords(int year, int month, int uid) {
        List<String> umoneyOut = new ArrayList<>();
        List<String> umoneyIn = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            if (month <= 0) {
                month = (12 + month);
                year--;
            }
            umoneyIn.add(0, String.format("%.2f", uMoneyService.getOneMonthUmoney(year, month, "流入", uid)));
            umoneyOut.add(0, String.format("%.2f", uMoneyService.getOneMonthUmoney(year, month, "流出", uid)));
            month--;
        }
        List<List<String>> list = new ArrayList<>();
        list.add(umoneyIn);
        list.add(umoneyOut);
        System.out.println("list = " + list);
        return list;
    }

}
