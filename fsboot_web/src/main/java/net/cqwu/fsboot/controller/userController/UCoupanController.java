package net.cqwu.fsboot.controller.userController;

import net.cqwu.fsboot.pojo.UCoupan;
import net.cqwu.fsboot.service.adminService.UCoupanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 18:39
 **/
@RestController
@RequestMapping("/ucoupan")
@CrossOrigin(allowedHeaders="*",value="*",originPatterns="*")
public class UCoupanController {
    @Autowired
    private UCoupanService u_coupanService;
    //查询用户优惠券列表
    @GetMapping("/getUCoupanList")
    public List<UCoupan> getUCoupanList(){
        System.out.println("getU_coupanList-----");
        return u_coupanService.list();
    }
    //给用户添加券
    @PostMapping("/addUCoupan")
    public String addU_coupan(@RequestBody UCoupan uCoupan){
        String mess = null;
        if (u_coupanService.getCoupanByUidAndCid(uCoupan.getUid(),uCoupan.getCid())==1){
            mess="用户已有该优惠券,不可重复拥有该优惠券";
            return mess;
        }else {
            u_coupanService.save(uCoupan);
            mess="发放成功";
            return mess;
        }
    }
    //用户用掉劵或者劵失效
    @DeleteMapping ("/deleteUCoupan/{uid},{cid}")
    public boolean deleteUCoupan(@PathVariable("uid")int uid,@PathVariable("cid")int cid){
        System.out.println("deleteUCoupan----");
        return u_coupanService.deleteById(uid,cid);
    }
    //根据用户id查找优惠券
    @GetMapping("/getUCoupanById/{uid}")
    public List<UCoupan> getUCoupanById(@PathVariable("uid")int uid){
        List<UCoupan> uCoupanList=u_coupanService.getUCoupanByIdList(uid);
        System.out.println("getUCoupanById-----"+uCoupanList);
        return uCoupanList;
    }
}
