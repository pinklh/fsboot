package net.cqwu.fsboot.controller.userController;

import net.cqwu.fsboot.pojo.*;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.AMoneyService;
import net.cqwu.fsboot.service.adminService.CoupanService;
import net.cqwu.fsboot.service.adminService.FruitService;
import net.cqwu.fsboot.service.adminService.LogisticService;
import net.cqwu.fsboot.service.userService.OrderService;
import net.cqwu.fsboot.service.userService.ShopCarService;
import net.cqwu.fsboot.service.userService.UMoneyService;
import net.cqwu.fsboot.service.userService.UsersService;
import org.apache.catalina.User;
import org.omg.CORBA.INTERNAL;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:35
 **/
@RestController
@RequestMapping("/order")
@CrossOrigin(allowedHeaders="*",value="*",originPatterns="*")
@Transactional
public class OrderController {
    @Resource
    private OrderService orderService;
    @Resource
    private UsersService usersService;
    @Resource
    private LogisticService logisticService;
    @Resource
    private UMoneyService uMoneyService;
    @Resource
    private ShopCarService shopCarService;
    @Resource
    private FruitService fruitService;
    @Resource
    private CoupanService coupanService;
    //查询订单列表
    @GetMapping("/getOrderList")
    public List<Order> getOrderList() {
        System.out.println("getOrderList----");
        return orderService.list();
    }

    /**
     * 从购物车中生成订单并将其生成了的删掉
     * @param order
     * @return
     */
    @PostMapping("/addOrder")
    public String addOrder(@RequestBody Order order) {
        System.out.println(order);
        System.out.println("进来了");
        DecimalFormat decimalFormat=new DecimalFormat(".00");
        float price = Float.parseFloat(decimalFormat.format(order.getOrprice()/order.getOprice()));
        System.out.println("addOrder------" + order);
        order.setOstate(0);
        Date date = new Date();
        order.setOtime(date);
        order.setOdiscount((price));
        order.setUid(order.getUid());
        List<ShopCar> shopCarByUid = shopCarService.getShopCarByUid(order.getUid());
        for (int i=0;i<shopCarByUid.size();i++){
            shopCarByUid.get(i).setSstatus(1);
            shopCarService.updateById(shopCarByUid.get(i));
        }

        List<ShopCar> carBy1 = shopCarService.getCarBy1(order.getUid());
        System.out.println("carBy1 = " + carBy1);
        for (int i =0;i<carBy1.size();i++){
            shopCarService.removeById(carBy1.get(i).getSid());
        }

        orderService.save(order);
        return "添加成功";
    }

    //修改订单信息
    @PutMapping("/updateOrder")
    public boolean updateOrder(@RequestBody Order order) {
        System.out.println("updateOrder----" + order);
        return orderService.updateById(order);
    }

    //删除订单
    @DeleteMapping("/deleteOrder/{oid}")
    public boolean deleteOrder(@PathVariable("oid") int oid) {
        System.out.println("deleteOrder----" + oid);
        return orderService.removeById(oid);
    }

    //根据订单id查找订单
    @GetMapping("/getOrderById/{oid}")
    public ResponseBody<Order> getOrderById(@PathVariable("oid") int oid) {
        int status = -1;
        String message = "未知错误";
        Order order = orderService.getById(oid);
        if (order != null) {
            status = 0;
        }
        return new ResponseBody<>(status, message, order);
    }

    //根据用户id查找订单
    @GetMapping("/getOrderByUid/{uid}")
    public List<Order> getOrderByUId(@PathVariable("uid") int uid) {
        return orderService.getOrderByUid(uid);
    }

    //付款逻辑
    @PostMapping("/fukuan")
    public String fukuan(@RequestBody Order order) {
        Users users = usersService.getById(order.getUid());
        Fruit fruit = fruitService.getFruitByFname(order.getOnote());
        float money = users.getUmoney() - order.getOrprice();
        UMoney uMoney = new UMoney();
        String mess;
        if (money < 0) {
            mess = "余额不足";
        } else {
            mess = "付款成功";
            if (order.getOcoupan()!=0){
                int cid =coupanService.getCidByCmoney(order.getOcoupan());
                coupanService.delByCidAndUid(cid,order.getUid());
            }
            int n = (int) (order.getOprice()/fruit.getFprice());
            fruit.setFstock(fruit.getFstock()-n);
            fruit.setFsale(fruit.getFsale()+n);
            fruitService.updateById(fruit);
            String note = users.getUname() + "买" + order.getOnote();
            uMoney.setMmoney(order.getOrprice());
            uMoney.setMuid(order.getUid());
            uMoney.setMnote(note);
            Date date = new Date();
            uMoney.setMtime(date);
            uMoney.setMtype("流出");
            uMoneyService.save(uMoney);
            users.setUmoney(money);
            usersService.updateById(users);
            order.setOstate(1);
            orderService.updateById(order);
        }
        return mess;
    }
    //收货逻辑
    @PostMapping("/shouhuo")
    public String shouhuo(@RequestBody Order order) {
        System.out.println("order.getOid() = " + order.getOid());
        Logistic logistic = logisticService.getLogByOid(order.getOid());
        logistic.setOstate(1);
        logisticService.updateById(logistic);
        order.setOstate(3);
        orderService.updateById(order);
        return "收货成功";
    }


    /**
     * 获得近六个月的销售额
     *
     * @return List
     */
    @GetMapping("/getSixMonthSales")
    public List<String> getSixMonthSales(int year, int month) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            if (month <= 0) {
                month = (12 + month);
                year--;
            }
            list.add(0, String.format("%.2f", orderService.getMonthSales(year, month)));
            month--;
        }
        return list;
    }
    /**
     * 查找用户不同状态的订单
     * @param ostate 状态
     * @param uid id
     * @return
     */
    @GetMapping("/orderNumber")
    public int orderNumber(int ostate,int uid){
        System.out.println("uid = " + uid);
        System.out.println("ostate = " + ostate);
        return orderService.orderNumber(ostate,uid);
    }
}
