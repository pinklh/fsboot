package net.cqwu.fsboot.controller.userController;


import net.cqwu.fsboot.pojo.*;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.AMoneyService;
import net.cqwu.fsboot.service.adminService.UCoupanService;
import net.cqwu.fsboot.service.userService.UMoneyService;
import net.cqwu.fsboot.service.userService.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 15:08
 **/
@RestController
@RequestMapping("/users")
@CrossOrigin(allowedHeaders="*",value="*",originPatterns="*")
public class UsersController {
    @Autowired
    private UsersService usersService  ;
    @Autowired
    private AMoneyService aMoneyService;
    @Autowired
    private UMoneyService uMoneyService;
    @Autowired
    private UCoupanService uCoupanService;
    //登录
    @GetMapping("/login")
    public ResponseBody<Users> login(@RequestParam String utele,
                                     @RequestParam String upwd
                                     ){
        Users users=usersService.login(utele,upwd);
        int status=-1;
        String message="用户名或密码错误";
        if(users!=null && users.getUid()>0){
            //登陆成功
            status=0;
            message="";

            if ( users.getEndday() != null ){
                if(new Date().after(users.getEndday())){
                    users.setUtype(0);
                    usersService.updateById(users);
                }
            }

            //将对象存储到session中
//            HttpSession session= request.getSession();
//            session.setAttribute("users" , users);
        }
        return new ResponseBody<>(status,message,users);
    }
    //获取用户列表
    @GetMapping("/getUsersList")
    public List<Users> getUsersList() {
        System.out.println("getUsersList----");
        return usersService.list();
    }
    //插入用户数据
    @PostMapping("/addUsers")
    public boolean addUsers(@RequestBody Users users){
        System.out.println("addUsers----"+users);
        return usersService.save(users);
    }
    //用户注册
    @GetMapping("/register")
    public boolean register(@RequestParam String utele,
                            @RequestParam String upwd,
                            @RequestParam String uname){
        System.out.println("register-----");
        return usersService.register(utele,upwd,uname);
    }
    //修改用户数据
    @PutMapping("/updateUsers")
    public boolean updateUsers(@RequestBody Users users){
        System.out.println("updateUsers----"+users);
        return usersService.updateById(users);
    }
    //前台用户修改用户数据
    @GetMapping("/updateUser")
    public boolean updateUser(@RequestParam int uid,
                                @RequestParam String utele,
                              @RequestParam String upwd,
                              @RequestParam String uname){
        System.out.println("updateUsers----"+uid);
        Users user=usersService.getById(uid);
        user.setUname(uname);
        user.setUtele(utele);
        user.setUpwd(upwd);
        System.out.println(user);
        return usersService.updateById(user);
    }
    //前台用户忘记密码,修改密码
    @GetMapping("/forgetPass")
    public boolean forgetPass(@RequestParam String utele,
                              @RequestParam String upwd,
                              @RequestParam String uname){
        Users user=usersService.selectByTeleName(utele,uname);
        user.setUpwd(upwd);
        System.out.println(user);
        return usersService.updateById(user);
    }

    //用户充值金额,管理员资金表多笔资金流入，用户资金表多笔充值记录
    @GetMapping("/rechargUser")
    public boolean rechargUser(@RequestParam int uid,
                               @RequestParam Float umoney){
        Users users=usersService.getById(uid);
        AMoney aMoney =new AMoney();
        UMoney umoney1=new UMoney();
        UCoupan uCoupan=new UCoupan();
        Date date = new Date();
        if(umoney>0){
            users.setUmoney(umoney+ users.getUmoney());
            aMoney.setMoney(umoney);
            aMoney.setNote("用户"+uid+"充值");
            aMoney.setAtime(date);
            aMoneyService.save(aMoney);
            umoney1.setMuid(uid);
            umoney1.setMtype("流入");
            umoney1.setMmoney(umoney);
            umoney1.setMtime(date);
            umoney1.setMnote("充值");
            uMoneyService.save(umoney1);
            System.out.println("充值成功，金额为"+umoney+",总金额为："+users.getUmoney());
        }
        if(umoney>=100 && umoney<200){
            if (uCoupanService.getCoupanByUidAndCid(uid,3)==0) {
                uCoupan.setUid(uid);
                uCoupan.setCid(3);
                uCoupanService.save(uCoupan);
            }
        }else if(umoney>=200 && umoney<300){
            if (uCoupanService.getCoupanByUidAndCid(uid,4)==0) {
                uCoupan.setUid(uid);
                uCoupan.setCid(4);
                uCoupanService.save(uCoupan);
            }
        }else if(umoney>=500 && umoney<600) {
            if (uCoupanService.getCoupanByUidAndCid(uid,5)==0) {
                uCoupan.setUid(uid);
                uCoupan.setCid(5);
                uCoupanService.save(uCoupan);
            }
        }
        return usersService.updateById(users);
    }
    //用户充值为超级会员
    @GetMapping("/changeUser")
    public boolean changeUser(@RequestParam int uid){
        Users users=usersService.getById(uid);
        float umoney=users.getUmoney();
        UMoney uMoney1=new UMoney();
        Date date=new Date();
        if(umoney-20>=0) {
            users.setUmoney(umoney-20);
            users.setUtype(1);
            users.setStartday(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            users.setEndday(calendar.getTime());
            uMoney1.setMuid(uid);
            uMoney1.setMmoney(20.0f);
            uMoney1.setMtime(date);
            uMoney1.setMtype("流出");
            uMoney1.setMnote("购买会员");
            uMoneyService.save(uMoney1);
        }
        return usersService.updateById(users);
    }
    //删除用户数据
    @DeleteMapping("/deleteUsers/{uid}")
    public boolean deleteUsers(@PathVariable("uid")int uid){
        System.out.println("deleteUsers----"+uid);
        return usersService.removeById(uid);
    }
    //根据id获取用户
    @GetMapping("/getUsersById/{uid}")
    public ResponseBody<Users> getUsersById(@PathVariable("uid")int uid){
        int status=-1;
        String message="未知错误";
        Users users=usersService.getById(uid);
        if(users!=null){
            status=0;
        }
        System.out.println("getUsersById----"+users);
        return new ResponseBody<>(status,message,users);
    }
//    多条件查询
    @RequestMapping("/conditionSelect")
    public List<Users> conditionSelectActivity(  String uname, int utype) {
        System.out.println("utype = " + utype);
        List<Users> users = usersService.conditionSelect(uname,utype);
        System.out.println(users);
        return users;
    }
    /**
     * 判断是否是会员
     * @return
     */
    @GetMapping("/isMember/{uid}")
    public Integer isMember(@PathVariable("uid") int uid){
        Users byId = usersService.getById(uid);
        return byId.getUtype();
    }

}
