package net.cqwu.fsboot.controller.adminController;

import net.cqwu.fsboot.pojo.Logistic;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.LogisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:07
 **/
@RestController
@RequestMapping("/logistic")
public class LogisticController {
    @Autowired
    private LogisticService logisticService;
    //查询物流列表
    @GetMapping("/getLogisticList")
    public List<Logistic> getLogisticList(){
        System.out.println("getLogisticList----");
        return logisticService.list();
    }
    //修改物流信息
    @PutMapping("/updateLoogistic")
    public boolean updateLoogistic(@RequestBody Logistic logistic){
        System.out.println("updateLoogistic-----"+logistic);
        return logisticService.save(logistic);
    }
    //添加物流信息
    @PostMapping("/addLogistic")
    public boolean addLogistic(@RequestBody Logistic logistic){
        System.out.println("addLogistic-----"+logistic);
        return logisticService.save(logistic);
    }
    //删除物流信息
    @DeleteMapping("/deleteLogistic/{lid}")
    public boolean deleteLogistic(@PathVariable("lid")int lid){
        System.out.println("deleteLogistic-----"+lid);
        return logisticService.removeById(lid);
    }
    //通过物流号查询物流信息
    @GetMapping("/getLogisticById/{lid}")
    public ResponseBody<Logistic> getLogisticById(@PathVariable("lid")int lid){
        int status=-1;
        String message="未知错误";
        Logistic logistic = logisticService.getById(lid);
        if(logistic !=null){
            status=0;
        }
        System.out.println("getLogisticById----"+logistic);
        return new ResponseBody<>(status,message,logistic);
    }
}
