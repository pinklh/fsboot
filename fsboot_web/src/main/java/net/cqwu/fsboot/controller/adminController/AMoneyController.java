package net.cqwu.fsboot.controller.adminController;

import net.cqwu.fsboot.pojo.AMoney;
import net.cqwu.fsboot.pojo.ResponseBody;
import net.cqwu.fsboot.service.adminService.AMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/5 11:25
 **/
@RestController
@RequestMapping("/amoney")
public class AMoneyController {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private AMoneyService aMoneyService;
    //查询资金收益列表
    @GetMapping("/getAMoneyList")
    public List<AMoney> getAMoneyList(){
        System.out.println("getAMoneyList----");
        return aMoneyService.list();
    }
    //添加资金收益
    @PostMapping("/addAMoney")
    public boolean addAMoney(@RequestBody AMoney aMoney){
        System.out.println("addAMoney-----");
        return aMoneyService.save(aMoney);
    }
    //删除资金收益
    @DeleteMapping("/deleteAMoney/{amid}")
    public boolean deleteAMoney(@PathVariable("amid")int amid){
        System.out.println("deleteAMoney-------"+amid);
        return aMoneyService.removeById(amid);
    }
    //通过资金收益id查询资金收益信息
    @GetMapping("/getAMoneyById/{amid}")
    public ResponseBody<AMoney> getAMoneyById(@PathVariable("amid")int amid){
        int status =-1;
        String message = "未知错误";
        AMoney aMoney=aMoneyService.getById(amid);
        if(aMoney!=null){
            status=0;
        }
        System.out.println("getAMoneyById----"+aMoney);
        return new ResponseBody<>(status,message,aMoney);
    }
    //计算资金收益总数
    @GetMapping("/getAllMoneySum")
    public ResponseBody<Float> getAllMoneySum() throws ParseException {
        int status=-1;
        String message="未知错误";
        float aMoneySum=aMoneyService.getAllMoenySum();
        if(aMoneySum!=0){
            status=0;
        }
//        System.out.println("getAllMoneySum----"+aMoneySum);
        return new ResponseBody<>(status,message,aMoneySum);
    }
}
