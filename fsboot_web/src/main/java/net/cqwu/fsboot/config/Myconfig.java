package net.cqwu.fsboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 不一样的黎
 * 我就是我，不一样的烟火！
 * @createdTime 2022/6/4 11:13
 */
@Configuration
public class Myconfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/*/*").allowedOrigins("*").allowedMethods("*").allowedHeaders("*").maxAge(2000);
    }

}
