/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80025
Source Host           : localhost:3306
Source Database       : fruitstore

Target Server Type    : MYSQL
Target Server Version : 80025
File Encoding         : 65001

Date: 2022-06-16 19:40:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `aid` int NOT NULL AUTO_INCREMENT,
  `aname` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apwd` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', '123');
INSERT INTO `admin` VALUES ('2', 'll', '123');
INSERT INTO `admin` VALUES ('3', 'txy', '123');
INSERT INTO `admin` VALUES ('4', 'skb', '123');
INSERT INTO `admin` VALUES ('5', 'clj', '123');
INSERT INTO `admin` VALUES ('6', 'lj', '123');
INSERT INTO `admin` VALUES ('7', 'lh', '123');

-- ----------------------------
-- Table structure for `a_money`
-- ----------------------------
DROP TABLE IF EXISTS `a_money`;
CREATE TABLE `a_money` (
  `amid` int NOT NULL AUTO_INCREMENT,
  `money` float NOT NULL,
  `note` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `atime` datetime NOT NULL,
  PRIMARY KEY (`amid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of a_money
-- ----------------------------
INSERT INTO `a_money` VALUES ('1', '500', '充值', '2022-06-05 00:00:00');
INSERT INTO `a_money` VALUES ('2', '300', '充值', '2022-06-05 00:00:00');
INSERT INTO `a_money` VALUES ('3', '600', '充值', '2022-06-05 00:00:00');
INSERT INTO `a_money` VALUES ('4', '29.8', '用户买水果', '2022-06-03 00:00:00');
INSERT INTO `a_money` VALUES ('21', '123', '123', '2022-06-30 00:00:00');
INSERT INTO `a_money` VALUES ('25', '62.3', '小花买香蕉', '2022-06-09 14:49:16');
INSERT INTO `a_money` VALUES ('26', '45', '小花买苹果', '2022-06-09 15:01:39');
INSERT INTO `a_money` VALUES ('27', '45', '小花买苹果', '2022-06-09 18:21:41');
INSERT INTO `a_money` VALUES ('28', '45', '小花买苹果', '2022-06-11 17:49:29');
INSERT INTO `a_money` VALUES ('29', '27.72', '小花买开心果', '2022-06-12 15:51:57');
INSERT INTO `a_money` VALUES ('30', '35.96', '小花买腰果', '2022-06-12 18:03:25');
INSERT INTO `a_money` VALUES ('31', '37.96', '小花买腰果', '2022-06-13 14:22:40');
INSERT INTO `a_money` VALUES ('32', '28.72', '小花买开心果', '2022-06-13 20:12:31');
INSERT INTO `a_money` VALUES ('33', '18.8', '小花买核桃', '2022-06-13 20:14:15');
INSERT INTO `a_money` VALUES ('34', '29.2', '小花买核桃', '2022-06-13 20:16:38');
INSERT INTO `a_money` VALUES ('35', '22.48', '小花买板栗', '2022-06-13 20:17:52');
INSERT INTO `a_money` VALUES ('36', '22.48', '小花买板栗', '2022-06-13 20:18:59');
INSERT INTO `a_money` VALUES ('37', '20', '用户1充值', '2022-06-13 21:10:05');
INSERT INTO `a_money` VALUES ('38', '20', '用户1充值', '2022-06-13 21:10:23');
INSERT INTO `a_money` VALUES ('39', '5000000', '用户2充值', '2022-06-14 15:11:39');
INSERT INTO `a_money` VALUES ('40', '20', '用户1充值', '2022-06-16 10:58:36');
INSERT INTO `a_money` VALUES ('41', '500', '用户1充值', '2022-06-16 10:58:48');
INSERT INTO `a_money` VALUES ('42', '500', '用户1充值', '2022-06-16 10:58:49');
INSERT INTO `a_money` VALUES ('43', '500', '用户1充值', '2022-06-16 10:58:50');
INSERT INTO `a_money` VALUES ('44', '500', '用户1充值', '2022-06-16 10:58:50');
INSERT INTO `a_money` VALUES ('45', '500', '用户1充值', '2022-06-16 10:58:50');
INSERT INTO `a_money` VALUES ('46', '500', '用户1充值', '2022-06-16 10:58:50');
INSERT INTO `a_money` VALUES ('47', '500', '用户1充值', '2022-06-16 10:58:51');
INSERT INTO `a_money` VALUES ('48', '500', '用户1充值', '2022-06-16 10:58:51');
INSERT INTO `a_money` VALUES ('49', '500', '用户1充值', '2022-06-16 10:58:51');
INSERT INTO `a_money` VALUES ('50', '50', '用户1充值', '2022-06-16 10:58:57');
INSERT INTO `a_money` VALUES ('51', '100', '用户1充值', '2022-06-16 10:59:06');
INSERT INTO `a_money` VALUES ('52', '200', '用户1充值', '2022-06-16 10:59:51');
INSERT INTO `a_money` VALUES ('53', '500', '用户1充值', '2022-06-16 11:00:00');
INSERT INTO `a_money` VALUES ('54', '500', '用户1充值', '2022-06-16 11:00:01');
INSERT INTO `a_money` VALUES ('55', '500', '用户1充值', '2022-06-16 11:00:01');
INSERT INTO `a_money` VALUES ('56', '500', '用户1充值', '2022-06-16 11:00:02');
INSERT INTO `a_money` VALUES ('57', '500', '用户1充值', '2022-06-16 11:00:02');
INSERT INTO `a_money` VALUES ('58', '500', '用户1充值', '2022-06-16 11:00:02');
INSERT INTO `a_money` VALUES ('59', '500', '用户1充值', '2022-06-16 11:00:02');
INSERT INTO `a_money` VALUES ('60', '500', '用户1充值', '2022-06-16 11:00:03');
INSERT INTO `a_money` VALUES ('61', '500', '用户1充值', '2022-06-16 11:00:03');
INSERT INTO `a_money` VALUES ('62', '500', '用户1充值', '2022-06-16 11:00:03');
INSERT INTO `a_money` VALUES ('63', '500', '用户1充值', '2022-06-16 11:00:04');
INSERT INTO `a_money` VALUES ('64', '500', '用户1充值', '2022-06-16 11:00:05');
INSERT INTO `a_money` VALUES ('65', '100', '用户1充值', '2022-06-16 19:33:09');
INSERT INTO `a_money` VALUES ('66', '100', '用户1充值', '2022-06-16 19:33:28');

-- ----------------------------
-- Table structure for `coupan`
-- ----------------------------
DROP TABLE IF EXISTS `coupan`;
CREATE TABLE `coupan` (
  `cid` int NOT NULL AUTO_INCREMENT,
  `cname` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cmoney` int NOT NULL,
  `cstartday` datetime NOT NULL,
  `cendday` datetime NOT NULL,
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of coupan
-- ----------------------------
INSERT INTO `coupan` VALUES ('1', '小额优惠券', '2', '2022-06-04 16:42:09', '2022-06-08 16:42:16');
INSERT INTO `coupan` VALUES ('2', '中等优惠券', '5', '2022-06-05 13:26:11', '2022-06-07 13:26:16');
INSERT INTO `coupan` VALUES ('3', '大额优惠券', '10', '2022-06-05 13:26:11', '2022-06-06 13:26:11');
INSERT INTO `coupan` VALUES ('5', '一等优惠', '50', '2022-06-07 13:26:11', '2022-06-10 13:26:11');
INSERT INTO `coupan` VALUES ('6', '二等优惠', '25', '2022-06-07 13:26:11', '2022-06-11 13:26:11');
INSERT INTO `coupan` VALUES ('7', '三等优惠', '10', '2022-06-07 13:26:11', '2022-06-11 13:26:11');
INSERT INTO `coupan` VALUES ('8', '测试', '6', '2022-06-13 12:00:00', '2022-06-23 12:00:00');
INSERT INTO `coupan` VALUES ('9', '四等优惠券', '1', '2022-06-15 12:00:00', '2022-06-16 12:00:00');

-- ----------------------------
-- Table structure for `fruits`
-- ----------------------------
DROP TABLE IF EXISTS `fruits`;
CREATE TABLE `fruits` (
  `fid` int NOT NULL AUTO_INCREMENT,
  `fname` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ftype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fnumber` int NOT NULL,
  `furl` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fdetail` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fprice` float NOT NULL,
  `fstock` int DEFAULT NULL,
  `fsale` int DEFAULT NULL,
  `fstatus` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`fid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of fruits
-- ----------------------------
INSERT INTO `fruits` VALUES ('2', '樱桃', '浆果类', '1', 'https://img95.699pic.com/photo/50021/5542.jpg_wh300.jpg', '维c之王，针叶樱桃', '120', '100', '20', '1');
INSERT INTO `fruits` VALUES ('3', '核桃', '坚果类', '1', 'https://img0.baidu.com/it/u=1644464002,1934060930&fm=253&fmt=auto&app=138&f=JPEG?w=753&h=500', '补脑', '13', '228', '12', '1');
INSERT INTO `fruits` VALUES ('4', '橘子', '柑橘类', '2', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.616pic.com%2Fys_bnew_img%2F00%2F34%2F79%2F6xJ0DMfT2a.jpg&refer=http%3A%2F%2Fpic.616pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657182974&t=82ca03faefa7423ad7a891b56bc7f8eb', '3富含维生素C', '2.7', '400', '23', '1');
INSERT INTO `fruits` VALUES ('5', '水蜜桃', '核果类', '68', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F078f3c54df8f7f195bc2edc704f024b5fb458bc91eacd-PUzfji_fw658&refer=http%3A%2F%2Fhbimg.b0.upaiyun.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183057&t=41115dbd1015fc1db494dc11efe6ad24', '4汁水多，好吃不腻', '5.8', '120', '55', '1');
INSERT INTO `fruits` VALUES ('6', '车厘子', '浆果类', '100', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp5.itc.cn%2Fq_70%2Fimages03%2F20210113%2F19a5251c60404c1ebe528e5fe280a477.png&refer=http%3A%2F%2Fp5.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183209&t=33d0ee00c32fa290ef3fac50a24755ac', '维生素C,花青素含量丰富', '13.8', '200', '33', '1');
INSERT INTO `fruits` VALUES ('7', '西瓜', '瓜果类', '150', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Ffwimage.cnfanews.com%2Fwebsiteimg%2F2015%2Fdata%2FAE5D8EC1BA03D538.jpg&refer=http%3A%2F%2Ffwimage.cnfanews.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183260&t=55e64770f0de0248018bf4e04ca28491', '皮薄汁水多，夏日解渴', '1.5', '600', '45', '1');
INSERT INTO `fruits` VALUES ('8', '芒果', '瓜果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fy.zdmimg.com%2F202105%2F11%2F6099f6d3461007025.jpg_d250.jpg&refer=http%3A%2F%2Fy.zdmimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183286&t=f7eb276c72f7be3b593f7ccc5283e133', '深色水果类，营养价值非常丰富，包含糖分、维生素B族、维生素C等多种维生素和膳食纤维', '5.6', '400', '56', '1');
INSERT INTO `fruits` VALUES ('9', '葡萄', '浆果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fupload.shejihz.com%2F2020%2F05%2Fbf1672b57c214af392210c795e868ddc.jpg&refer=http%3A%2F%2Fupload.shejihz.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183319&t=6898242e8d9f20561e39b03642badc41', '含有果糖、苹果酸、多种氨基酸以及维生素，还含有生物活性物质花青素', '4.7', '380', '74', '1');
INSERT INTO `fruits` VALUES ('10', '荔枝', '核果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimagepphcloud.thepaper.cn%2Fpph%2Fimage%2F132%2F555%2F352.jpg&refer=http%3A%2F%2Fimagepphcloud.thepaper.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183339&t=1545c64bb2311deb46bddb6c56eed131', '含有丰富的糖分，有补充能量，增强营养的作用', '8.8', '280', '86', '1');
INSERT INTO `fruits` VALUES ('11', '李子', '核果类', '150', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F110420104302%2F201104104302-1-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183373&t=0ef72f809f8f84e1023bbe1d7d8036dd', '能促进胃酸和胃消化酶的分泌，有增加肠胃蠕动的作用，能促进消化，增加食欲', '2.1', '400', '23', '1');
INSERT INTO `fruits` VALUES ('12', '柚子', '柑橘类', '100', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.suoyanzi.com%2Fwp-content%2Fuploads%2F2021%2F12%2F1639184301787_1.jpeg&refer=http%3A%2F%2Fwww.suoyanzi.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183402&t=6d49b4fa92523b4130d91ec06cd4f56e', '含有糖类、维生素B1、维生素B2、维生素C、维生素P、胡萝卜素、钾、钙、磷、枸橼酸等', '1.5', '450', '45', '1');
INSERT INTO `fruits` VALUES ('13', '苹果', '瓜果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F102320095002%2F201023095002-1-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183426&t=b19dcbd39523a9b88248ecc590942b7e', '含有丰富的维生素C和维生素E，有利于促进细胞抗氧化，清除自由基，还可减少皱纹，可有效美白肌肤', '5.3', '300', '36', '1');
INSERT INTO `fruits` VALUES ('14', '凤梨', '瓜果类', '100', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13250505834%2F1000.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183451&t=d325cb0924ba06874ef0f9334502a84d', '含有多种矿物质和维生素，有利于脂肪的消耗，有一定的减肥作用', '6.6', '500', '48', '1');
INSERT INTO `fruits` VALUES ('15', '菠萝', '瓜果类', '120', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic3.zhimg.com%2Fv2-6caf0afd9e779257752a7f00b8f53642_r.jpg&refer=http%3A%2F%2Fpic3.zhimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183471&t=3ee2a6e20dfb81fafe6d1b38ee2448bf', '含有大量的果糖，葡萄糖，维生素B、C，磷，柠檬酸和蛋白酶等物', '7.3', '200', '13', '1');
INSERT INTO `fruits` VALUES ('16', '开心果', '坚果类', '288', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fn.sinaimg.cn%2Fsinakd10123%2F600%2Fw1500h1500%2F20211025%2Fe0dc-4fc0f14acd189f11bcf0fe777c40ae5c.jpg&refer=http%3A%2F%2Fn.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183490&t=5d582835813ad642d22764c4117a6854', '滋养身体，可以降低血脂，抗动脉粥样硬化', '19.2', '295', '3', '1');
INSERT INTO `fruits` VALUES ('17', '板栗', '坚果类', '300', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fcdn.img.fagua.net%2Fg3img%2Ftsqianghang1%2Fc2_20200826093534_10038.jpg&refer=http%3A%2F%2Fcdn.img.fagua.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183513&t=a5cb9635df1d0174ec3edb032ffd184c', '一种补养治病的保养品，有养胃、健脾、补肾、壮腰、强健壮体、止血消肿的功效', '10.2', '0', '5', '0');
INSERT INTO `fruits` VALUES ('18', '杏仁', '坚果类', '288', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp5.itc.cn%2Fimages01%2F20210528%2F42bc5e02bb234c1db36df463eb42ab04.jpeg&refer=http%3A%2F%2Fp5.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183537&t=5d25e7070cb790bea72619841bc7ec9e', '杏仁钙和维生素E含量较高', '13.4', '394', '3', '1');
INSERT INTO `fruits` VALUES ('19', '腰果', '坚果类', '300', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg-pre.ivsky.com%2Fimg%2Ftupian%2Fpre%2F202001%2F31%2Fyaoguo_png_sucai-001.png&refer=http%3A%2F%2Fimg-pre.ivsky.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183558&t=f418b4cde2dec2d20dceacd653c9837f', '腰果有相对高的蛋白质和较高的矿物质铁、锌、镁', '12.8', '5', '34', '1');
INSERT INTO `fruits` VALUES ('20', '巴西坚果', '坚果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.mp.itc.cn%2Fupload%2F20160928%2Fc862b2cb11be42f29bc8c76915c26079_th.jpg&refer=http%3A%2F%2Fimg.mp.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183581&t=b24a153828941f0d558dc61f50f02531', '巴西坚果是很好的矿物质硒来源', '8.7', '200', '0', '1');
INSERT INTO `fruits` VALUES ('21', '树莓', '浆果类', '150', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F122220143I2%2F201222143I2-3-lp.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183620&t=6c9b1d52919fabb0d57a6cbaf9217a50', '、树莓具有滋补肾脏的作用，可用于治疗因肾虚所致的腰膝酸软、神疲乏力、少气懒言', '12.5', '400', '67', '1');
INSERT INTO `fruits` VALUES ('22', '蓝莓', '浆果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.sccnn.com%2Fbimg%2F340%2F02034.jpg&refer=http%3A%2F%2Fimg.sccnn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183643&t=7dd6813ae3d5111e7dbde10b4531c1af', '含有大量的花青素，大量的氨基酸以及多糖类的物质，所以蓝莓可以起到保护心脑血管的作用', '10', '500', '28', '1');
INSERT INTO `fruits` VALUES ('23', '桑葚', '浆果类', '156', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13359315405%2F1000.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183700&t=995de677f33f87817a0cb8494eb12cc1', '生津止渴、润燥滑肠。桑葚性味甘、寒,入肝、肾经,既滋补肝肾,又能生津止渴、润燥滑肠,', '5.8', '300', '68', '1');
INSERT INTO `fruits` VALUES ('24', '草莓', '浆果类', '150', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fcontent.pic.tianqistatic.com%2Fcontent%2Ftoutiao%2Fimages%2F202104%2F02%2Fd54d701e2e99abc5.jpg%2Ftqjia&refer=http%3A%2F%2Fcontent.pic.tianqistatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183728&t=39a897fd224e4f75939fcd440d959835', '。具有润肺生津，健脾，消暑，解热，利尿，止渴的功效', '15.3', '400', '27', '1');
INSERT INTO `fruits` VALUES ('25', '柑子', '柑橘类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp8.itc.cn%2Fimages01%2F20210319%2Fe4f77a22fe8843ada3b5b44581c84501.jpeg&refer=http%3A%2F%2Fp8.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183756&t=b77782bc93db99743e2d6cfdbc0d93f3', '具有生津止渴，消炎杀菌，防癌抗癌的功效', '2.2', '600', '78', '1');
INSERT INTO `fruits` VALUES ('26', '桔子', '柑橘类', '120', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.616pic.com%2Fys_bnew_img%2F00%2F10%2F10%2Fd3pbUEI8DO.jpg&refer=http%3A%2F%2Fpic.616pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183782&t=15b34881d064828ce58da1be44e55c45', '降火降压', '2.5', '650', '33', '1');
INSERT INTO `fruits` VALUES ('27', '沙糖桔', '柑橘类', '150', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fnynct.gxzf.gov.cn%2Fxwdt%2Fgxlb%2Flb%2FW020201127566085474737.png&refer=http%3A%2F%2Fnynct.gxzf.gov.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183809&t=e0d00d024ba399ee78a7dbbb4fcf3605', '有理气化痰、健脾开胃的作用', '2.6', '700', '89', '1');
INSERT INTO `fruits` VALUES ('28', '血橙', '柑橘类', '290', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg12.360buyimg.com%2Fn0%2Fjfs%2Ft1%2F161118%2F29%2F20202%2F375708%2F607f7964Ee2d68ae0%2F63b5a8187ac50183.jpg&refer=http%3A%2F%2Fimg12.360buyimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183839&t=13a85f3f5fad8e82ede5ccaf432fa693', '富含维生素C、维生素P、膳食纤维、钾、果胶等', '3.2', '750', '100', '1');
INSERT INTO `fruits` VALUES ('29', '梅子', '核果类', '231', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fseopic.699pic.com%2Fphoto%2F50125%2F1278.jpg_wh1200.jpg&refer=http%3A%2F%2Fseopic.699pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183874&t=2d7b735e949b8751cc85af3688486e16', '梅子可入肝经,具有滋补肝脏的作用', '11.2', '800', '30', '1');
INSERT INTO `fruits` VALUES ('30', '枣子', '核果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fitbbs%2F1308%2F27%2Fc7%2F24937510_1377567140044_mthumb.jpg&refer=http%3A%2F%2Fimg.pconline.com.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183899&t=44e91651f3f786a4771ed695f173f81f', '补中益气,养血安神,缓和药性', '3.2', '600', '70', '1');
INSERT INTO `fruits` VALUES ('31', '花生', '坚果类', '200', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FAKL64v7fZHJZ1GaZETcm5A.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657271802&t=262f9faf181242218fd0b83ecc3a2fbf', '保健、促进大脑发育、抗衰老、预防肿瘤', '2.8', '498', '2', '1');
INSERT INTO `fruits` VALUES ('32', '葵花子', '坚果类', '300', 'http://t14.baidu.com/it/u=1470334654,1382374577&fm=224&app=112&f=JPEG?w=500&h=500', ' 润滑皮肤、助眠、增加人体记忆能力', '5.5', '596', '4', '1');
INSERT INTO `fruits` VALUES ('33', '坚果类总图', '坚果类', '1000', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg2.bjd.com.cn%2F2021%2F10%2F15%2Fd6ccc6564bfdb271c760a343067acbaa2312b1ab.jpeg&refer=http%3A%2F%2Fimg2.bjd.com.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657271973&t=fe672b0c0b3f893f09849964f97423eb', '有利于身体健康', '9', '100', '34', '1');
INSERT INTO `fruits` VALUES ('34', '松子', '坚果类', '300', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FT3s8-9tznh4l1q40ec9e3Q.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657272069&t=d156f5bb8e34f9bba2be6ec5fc0cbfcf', '一种经常食用的坚果,性平、味甘,归于肺、大肠经', '11.1', '200', '20', '1');
INSERT INTO `fruits` VALUES ('35', '莲子', '坚果类', '259', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.5h.com%2Fd%2Ffile%2F20210810%2F1628566895669980.jpeg&refer=http%3A%2F%2Fwww.5h.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657272191&t=81173489f073faee27158496c276b9fb', '一种药食同源的中药，就是既可以作为食物来食用，也可以入药', '12.8', '20', '2', '1');
INSERT INTO `fruits` VALUES ('36', '南瓜子', '坚果类', '290', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fi2%2F353318131%2FO1CN01MAz7xR29w31EKxyEn_%21%21353318131.jpg&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657272282&t=5985329ae288521899c18c0b98dc0477', '具有利水通淋、解毒、杀虫的作用', '1.5', '200', '18', '0');

-- ----------------------------
-- Table structure for `logistic`
-- ----------------------------
DROP TABLE IF EXISTS `logistic`;
CREATE TABLE `logistic` (
  `lid` int NOT NULL AUTO_INCREMENT,
  `oid` int NOT NULL,
  `oaddress` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `omessage` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ostate` int NOT NULL,
  PRIMARY KEY (`lid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of logistic
-- ----------------------------
INSERT INTO `logistic` VALUES ('18', '2', '河北张家口1', '苹果', '1');
INSERT INTO `logistic` VALUES ('19', '19', '测试', '开心果', '1');
INSERT INTO `logistic` VALUES ('20', '23', '测试', '腰果', '0');
INSERT INTO `logistic` VALUES ('21', '22', '啊实打实', '腰果', '0');
INSERT INTO `logistic` VALUES ('22', '32', '测试', '杏仁', '1');
INSERT INTO `logistic` VALUES ('23', '33', '按测试·', '开心果', '0');
INSERT INTO `logistic` VALUES ('24', '34', '测试花生', '花生', '0');
INSERT INTO `logistic` VALUES ('25', '53', '重庆文理学院', '核桃', '1');

-- ----------------------------
-- Table structure for `ordera`
-- ----------------------------
DROP TABLE IF EXISTS `ordera`;
CREATE TABLE `ordera` (
  `oid` int NOT NULL AUTO_INCREMENT,
  `ostate` int NOT NULL DEFAULT '0',
  `omessage` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `oprice` float NOT NULL,
  `onote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ocoupan` int NOT NULL,
  `odiscount` float NOT NULL,
  `uid` int NOT NULL,
  `otime` datetime NOT NULL,
  `orprice` float NOT NULL,
  PRIMARY KEY (`oid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ordera
-- ----------------------------
INSERT INTO `ordera` VALUES ('2', '4', '河北张家口1', '50', '苹果', '5', '0.9', '1', '2022-06-09 00:00:00', '45');
INSERT INTO `ordera` VALUES ('34', '0', '测试花生', '5.6', '花生', '2', '0.44', '2', '2022-06-14 09:10:00', '2.48');
INSERT INTO `ordera` VALUES ('39', '4', '啊实打实大', '26.1', '巴西坚果', '2', '0.72', '1', '2022-06-14 14:57:40', '18.88');
INSERT INTO `ordera` VALUES ('40', '1', '小小', '17.4', '巴西坚果', '2', '0.69', '2', '2022-06-14 15:02:50', '11.92');
INSERT INTO `ordera` VALUES ('41', '5', '用户一测试优惠券', '20.4', '板栗', '2', '0.9', '1', '2022-06-14 19:36:34', '18.4');
INSERT INTO `ordera` VALUES ('42', '5', '测试', '20.4', '板栗', '1', '0.95', '1', '2022-06-16 08:44:33', '19.4');
INSERT INTO `ordera` VALUES ('43', '5', '啊实打实大', '17.4', '巴西坚果', '50', '-1.87', '1', '2022-06-16 08:51:38', '-32.6');
INSERT INTO `ordera` VALUES ('44', '4', '打打', '64', '莲子', '0', '1', '1', '2022-06-16 08:53:08', '64');
INSERT INTO `ordera` VALUES ('45', '1', '123113', '23.8', '葵花子', '0', '0.37', '1', '2022-06-16 11:21:32', '8.8');
INSERT INTO `ordera` VALUES ('46', '1', '艾维奇翁', '25.6', '莲子', '2', '0.72', '1', '2022-06-16 11:30:09', '18.48');
INSERT INTO `ordera` VALUES ('49', '0', '啊实打实大', '38.4', '开心果', '0', '0.8', '1', '2022-06-16 14:46:28', '30.72');
INSERT INTO `ordera` VALUES ('51', '1', '数学擦拭的法孙菲菲', '40.2', '杏仁', '0', '0.8', '1', '2022-06-16 15:28:49', '32.16');
INSERT INTO `ordera` VALUES ('53', '3', '重庆文理学院', '26', '核桃', '2', '0.72', '1', '2022-06-16 18:23:45', '18.8');
INSERT INTO `ordera` VALUES ('56', '0', 'sadasd ', '38.4', '开心果', '50', '-0.5', '1', '2022-06-16 19:04:29', '-19.28');
INSERT INTO `ordera` VALUES ('57', '1', 'asdasdasda', '19.2', '开心果', '50', '0', '1', '2022-06-16 19:05:54', '0');

-- ----------------------------
-- Table structure for `refund`
-- ----------------------------
DROP TABLE IF EXISTS `refund`;
CREATE TABLE `refund` (
  `rid` int NOT NULL AUTO_INCREMENT,
  `oid` int NOT NULL,
  `uid` int NOT NULL,
  `reason` varchar(255) NOT NULL,
  `rmoney` float NOT NULL,
  `rstatus` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of refund
-- ----------------------------
INSERT INTO `refund` VALUES ('1', '41', '1', '不想要了', '18.4', '1');
INSERT INTO `refund` VALUES ('2', '39', '1', '不喜欢这个，拍错了', '18.88', '1');
INSERT INTO `refund` VALUES ('3', '2', '1', '未按约定时间发货', '45', '0');
INSERT INTO `refund` VALUES ('4', '42', '1', '7天无理由退货', '19.4', '1');
INSERT INTO `refund` VALUES ('5', '43', '1', '多拍、错拍', '-32.6', '1');
INSERT INTO `refund` VALUES ('6', '44', '1', '多拍、错拍', '64', '0');

-- ----------------------------
-- Table structure for `shopcar`
-- ----------------------------
DROP TABLE IF EXISTS `shopcar`;
CREATE TABLE `shopcar` (
  `sid` int NOT NULL AUTO_INCREMENT,
  `fid` int NOT NULL,
  `uid` int NOT NULL,
  `furl` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fname` varchar(35) NOT NULL,
  `sprice` float NOT NULL,
  `snumber` int NOT NULL,
  `price` float NOT NULL,
  `sstatus` int NOT NULL DEFAULT '0',
  `fstatus` int DEFAULT '1',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of shopcar
-- ----------------------------
INSERT INTO `shopcar` VALUES ('74', '17', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fcdn.img.fagua.net%2Fg3img%2Ftsqianghang1%2Fc2_20200826093534_10038.jpg&refer=http%3A%2F%2Fcdn.img.fagua.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657183513&t=a5cb9635df1d0174ec3edb032ffd184c', '板栗', '10.2', '2', '20.4', '0', '0');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `uname` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `upwd` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `utele` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `utype` int NOT NULL,
  `startday` datetime DEFAULT NULL,
  `endday` datetime DEFAULT NULL,
  `umoney` float NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '刘佳', '123', '15343131713', '1', '2022-06-16 11:12:11', '2022-07-16 11:12:11', '926.16');
INSERT INTO `users` VALUES ('2', '小小', '123', '15583202678', '1', '2022-06-05 00:00:00', '2022-06-06 00:00:00', '5000300');
INSERT INTO `users` VALUES ('3', '张三', '123', '18581795587', '1', '2022-06-06 00:00:00', '2022-06-07 00:00:00', '600');

-- ----------------------------
-- Table structure for `u_coupan`
-- ----------------------------
DROP TABLE IF EXISTS `u_coupan`;
CREATE TABLE `u_coupan` (
  `uid` int NOT NULL,
  `cid` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`cid`,`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of u_coupan
-- ----------------------------
INSERT INTO `u_coupan` VALUES ('1', '1');
INSERT INTO `u_coupan` VALUES ('2', '2');
INSERT INTO `u_coupan` VALUES ('3', '2');
INSERT INTO `u_coupan` VALUES ('1', '3');

-- ----------------------------
-- Table structure for `u_money`
-- ----------------------------
DROP TABLE IF EXISTS `u_money`;
CREATE TABLE `u_money` (
  `mid` int NOT NULL AUTO_INCREMENT,
  `m_uid` int NOT NULL,
  `mtype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `m_money` float NOT NULL,
  `mtime` datetime NOT NULL,
  `mnote` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`mid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of u_money
-- ----------------------------
INSERT INTO `u_money` VALUES ('1', '1', '流出', '29.8', '2022-06-03 00:00:00', '买水果');
INSERT INTO `u_money` VALUES ('2', '27', '流入', '20', '2022-06-11 21:52:12', '充值');
INSERT INTO `u_money` VALUES ('3', '27', '流入', '30', '2022-06-12 10:08:09', '充值');
INSERT INTO `u_money` VALUES ('4', '27', '流入', '50', '2022-06-12 10:18:48', '充值');
INSERT INTO `u_money` VALUES ('5', '4', '流出', '20', '2022-06-12 10:26:00', '充值会员');
INSERT INTO `u_money` VALUES ('6', '4', '流入', '20', '2022-06-12 10:27:25', '充值');
INSERT INTO `u_money` VALUES ('7', '1', '流出', '30.16', '2022-06-13 20:32:24', '小花买杏仁');
INSERT INTO `u_money` VALUES ('8', '1', '流出', '28.72', '2022-06-13 21:05:53', '小花买开心果');
INSERT INTO `u_money` VALUES ('9', '1', '流入', '20', '2022-06-13 21:10:05', '充值');
INSERT INTO `u_money` VALUES ('10', '1', '流入', '20', '2022-06-13 21:10:23', '充值');
INSERT INTO `u_money` VALUES ('11', '1', '流出', '2.48', '2022-06-14 09:10:17', '小花买花生');
INSERT INTO `u_money` VALUES ('12', '2', '流入', '5000000', '2022-06-14 15:11:39', '充值');
INSERT INTO `u_money` VALUES ('13', '1', '流出', '18.4', '2022-06-14 19:36:43', '小花买板栗');
INSERT INTO `u_money` VALUES ('14', '1', '流出', '18.88', '2022-06-15 14:34:18', '刘佳买巴西坚果');
INSERT INTO `u_money` VALUES ('15', '1', '刘佳退款', '18.4', '2022-06-15 16:37:47', '流入');
INSERT INTO `u_money` VALUES ('16', '1', '刘佳退款', '18.4', '2022-06-15 16:40:29', '流入');
INSERT INTO `u_money` VALUES ('17', '1', '刘佳退款', '18.4', '2022-06-15 16:42:57', '流入');
INSERT INTO `u_money` VALUES ('18', '1', '刘佳退款', '18.88', '2022-06-15 16:45:41', '流入');
INSERT INTO `u_money` VALUES ('19', '1', '流出', '19.4', '2022-06-16 08:44:48', '刘佳买板栗');
INSERT INTO `u_money` VALUES ('20', '1', '刘佳退款', '19.4', '2022-06-16 08:48:07', '流入');
INSERT INTO `u_money` VALUES ('21', '1', '流出', '-32.6', '2022-06-16 08:51:57', '刘佳买巴西坚果');
INSERT INTO `u_money` VALUES ('22', '1', '刘佳退款', '-32.6', '2022-06-16 08:52:18', '流入');
INSERT INTO `u_money` VALUES ('23', '1', '流出', '64', '2022-06-16 10:58:10', '刘佳买莲子');
INSERT INTO `u_money` VALUES ('24', '1', '流入', '20', '2022-06-16 10:58:36', '充值');
INSERT INTO `u_money` VALUES ('25', '1', '流入', '500', '2022-06-16 10:58:48', '充值');
INSERT INTO `u_money` VALUES ('26', '1', '流入', '500', '2022-06-16 10:58:49', '充值');
INSERT INTO `u_money` VALUES ('27', '1', '流入', '500', '2022-06-16 10:58:50', '充值');
INSERT INTO `u_money` VALUES ('28', '1', '流入', '500', '2022-06-16 10:58:50', '充值');
INSERT INTO `u_money` VALUES ('29', '1', '流入', '500', '2022-06-16 10:58:50', '充值');
INSERT INTO `u_money` VALUES ('30', '1', '流入', '500', '2022-06-16 10:58:50', '充值');
INSERT INTO `u_money` VALUES ('31', '1', '流入', '500', '2022-06-16 10:58:51', '充值');
INSERT INTO `u_money` VALUES ('32', '1', '流入', '500', '2022-06-16 10:58:51', '充值');
INSERT INTO `u_money` VALUES ('33', '1', '流入', '500', '2022-06-16 10:58:51', '充值');
INSERT INTO `u_money` VALUES ('34', '1', '流入', '50', '2022-06-16 10:58:57', '充值');
INSERT INTO `u_money` VALUES ('35', '1', '流入', '100', '2022-06-16 10:59:06', '充值');
INSERT INTO `u_money` VALUES ('36', '1', '流入', '200', '2022-06-16 10:59:51', '充值');
INSERT INTO `u_money` VALUES ('37', '1', '流入', '500', '2022-06-16 11:00:00', '充值');
INSERT INTO `u_money` VALUES ('38', '1', '流入', '500', '2022-06-16 11:00:01', '充值');
INSERT INTO `u_money` VALUES ('39', '1', '流入', '500', '2022-06-16 11:00:01', '充值');
INSERT INTO `u_money` VALUES ('40', '1', '流入', '500', '2022-06-16 11:00:02', '充值');
INSERT INTO `u_money` VALUES ('41', '1', '流入', '500', '2022-06-16 11:00:02', '充值');
INSERT INTO `u_money` VALUES ('42', '1', '流入', '500', '2022-06-16 11:00:02', '充值');
INSERT INTO `u_money` VALUES ('43', '1', '流入', '500', '2022-06-16 11:00:02', '充值');
INSERT INTO `u_money` VALUES ('44', '1', '流入', '500', '2022-06-16 11:00:03', '充值');
INSERT INTO `u_money` VALUES ('45', '1', '流入', '500', '2022-06-16 11:00:03', '充值');
INSERT INTO `u_money` VALUES ('46', '1', '流入', '500', '2022-06-16 11:00:03', '充值');
INSERT INTO `u_money` VALUES ('47', '1', '流入', '500', '2022-06-16 11:00:04', '充值');
INSERT INTO `u_money` VALUES ('48', '1', '流入', '500', '2022-06-16 11:00:05', '充值');
INSERT INTO `u_money` VALUES ('49', '1', '刘佳退款', '18.4', '2022-06-16 11:10:55', '流入');
INSERT INTO `u_money` VALUES ('50', '1', '流出', '20', '2022-06-16 11:12:11', '购买会员');
INSERT INTO `u_money` VALUES ('51', '1', '流出', '8.8', '2022-06-16 11:21:53', '刘佳买葵花子');
INSERT INTO `u_money` VALUES ('52', '1', '流出', '18.48', '2022-06-16 11:30:24', '刘佳买莲子');
INSERT INTO `u_money` VALUES ('53', '1', '流出', '32.16', '2022-06-16 15:29:08', '刘佳买杏仁');
INSERT INTO `u_money` VALUES ('54', '1', '流出', '18.8', '2022-06-16 18:23:57', '刘佳买核桃');
INSERT INTO `u_money` VALUES ('55', '1', '流出', '0', '2022-06-16 19:06:22', '刘佳买开心果');
INSERT INTO `u_money` VALUES ('56', '1', '流入', '100', '2022-06-16 19:33:09', '充值');
INSERT INTO `u_money` VALUES ('57', '1', '流入', '100', '2022-06-16 19:33:28', '充值');
