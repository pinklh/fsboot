package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/5 15:44
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("admin")
public class Admin {
    //管理员id
    @TableId(value = "aid",type = IdType.AUTO)
    private Integer aid;
    //管理员名称
    private String aname;
    //管理员密码
    private String apwd;
}
