package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 14:56
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("users")
public class Users {
    //用户ID
    @TableId(value = "uid",type = IdType.AUTO)
    private Integer uid;
    //用户名
    @TableField("uname")
    private String uname;
    //用户密码
    @TableField("upwd")
    private String upwd;
    //用户电话
    @TableField("utele")
    private String utele;
    //用户类型
    @TableField("utype")
    private Integer utype;
    //会员开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date startday;
    //会员结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date endday;
    //余额
    @TableField("umoney")
    private Float umoney;
}
