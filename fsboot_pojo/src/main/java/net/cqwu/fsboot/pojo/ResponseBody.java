package net.cqwu.fsboot.pojo;

import lombok.Data;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 10:58
 **/
@Data
public class ResponseBody<T> {
    private Integer status;
    private String message;
    private T data;
    public ResponseBody(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
