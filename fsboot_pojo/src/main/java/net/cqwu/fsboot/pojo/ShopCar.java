package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/9 19:19
 * @PackageName:net.cqwu.fsboot.pojo
 * @ClassName: ShopCar
 * @Description: TODO
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("shopcar")
public class ShopCar {
    /**
        购物车主键
    */
    @TableId(value = "sid",type = IdType.AUTO)
    private Integer sid;
    /**
        水果id
    */
    private Integer fid;
    /**
        用户id
    */
    private Integer uid;
    /**
        水果名称
    */

    private String fname;
    /**
        购买数量
    */
    private Integer snumber;
    private Float price;
    /**
        状态码 用来生成订单时判断
    */
    private Integer sstatus;
    /**
        用于展示图片
    */

    private String furl;
    /**
     * 单价
     */
    private Float sprice;
    /**
        用于假删除
    */
    private Integer fstatus ;
    
}
