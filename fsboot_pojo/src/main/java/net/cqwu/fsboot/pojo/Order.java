package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 17:22
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("ordera")
public class Order {
    //订单号
    @TableId(value = "oid",type = IdType.AUTO)
    private Integer oid;
    //订单状态
    private Integer ostate;
    //订单详情
    @TableField("omessage")
    private String omessage;
    //订单总额
    private Float oprice;
    //订单留言
    private String onote;
    //使用优惠券
    private Integer ocoupan;
    //订单折扣
    private Float odiscount;
    //用户id
    private Integer uid;
    //创建订单时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date otime;
    //订单实付金额
    private Float orprice;
}
