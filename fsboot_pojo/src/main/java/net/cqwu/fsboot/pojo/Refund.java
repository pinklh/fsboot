package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author 打工仔小刘
 * 努力在网上搬砖
 * @Date 2022/6/15 15:10
 * @PackageName:net.cqwu.fsboot.pojo
 * @ClassName: Refund
 * @Description: TODO
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("refund")
public class Refund {
    /**
        退款id
    */
    @TableId(value = "rid",type = IdType.AUTO)
    private Integer rid;
    /**
        退款订单
    */
    private Integer oid;
    /**
        退款用户
    */
    private Integer uid;
    /**
        退款理由
    */
    
    private String reason;
    private Float rmoney;
    /**
        退款状态
    */
    private Integer rstatus;
}
