package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/5 11:15
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("a_money")
public class AMoney {
    //收益id
    @TableId(value = "amid",type = IdType.AUTO)
    private Integer amid;
    //收益金额
    private Float money;
    //收益类型
    private String note;
    //时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date atime;
}
