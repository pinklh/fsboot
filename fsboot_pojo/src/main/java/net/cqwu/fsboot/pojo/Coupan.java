package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 12:48
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("coupan")
public class Coupan {
    //优惠券id
    @TableId(value = "cid",type = IdType.AUTO)
    private Integer cid;
    //优惠券名称
    private String cname;
    private Integer cmoney;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date cstartday;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date cendday;
}
