package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 18:32
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("u_coupan")
public class UCoupan {
    @TableId(value = "uid")
    private Integer uid;
    private Integer cid;
}
