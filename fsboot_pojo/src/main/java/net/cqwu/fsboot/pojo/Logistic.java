package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 16:59
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("logistic")
public class Logistic {
    //物流号
    @TableId(value = "lid",type = IdType.AUTO)
    private Integer lid;
    //订单号
    private Integer oid;
    //物流地址
    private String oaddress;
    //物流详情
    private String omessage;
    //物流状态
    private Integer ostate;
}
