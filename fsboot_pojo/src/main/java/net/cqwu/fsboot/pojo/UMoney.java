package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/4 19:21
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("u_money")
public class UMoney {
    //资金id
    @TableId(value = "mid",type = IdType.AUTO)
    private Integer mid;
    //用户id
    @TableField("m_uid")
    private Integer muid;
    //资金流入流出类型
    private String mtype;
    //流入流出金额
    @TableField("m_money")
    private Float mmoney;
    //资金流入流出时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date mtime;
    //资金详情备注
    private String mnote;

}
