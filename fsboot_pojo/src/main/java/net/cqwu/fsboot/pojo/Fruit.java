package net.cqwu.fsboot.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Author:耿直小随
 * 我不是无情的敲码机器 而是我自己游戏的创作者
 * Date:2022/6/3 22:34
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("fruits")
public class Fruit {
    //水果id
    @TableId(value = "fid",type = IdType.AUTO)
    private Integer fid;
    //水果名字
    @TableField("fname")
    private String fname;
    //水果类型
    @TableField("ftype")
    private String ftype;
    //水果数量
    @TableField("fnumber")
    private Integer fnumber;
    //水果照片路径
    @TableField("furl")
    private String furl;
    //水果详情
    @TableField("fdetail")
    private String fdetail;
    //水果价格
    @TableField("fprice")
    private Float fprice;
    //水果库存
    @TableField("fstock")
    private Integer fstock;
    //水果销量
    @TableField("fsale")
    private Integer fsale;
    /**
     用于假删除
     */
    private Integer fstatus ;
}
